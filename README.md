# Fusion Engine

Fusion Engine is an open source Game Engine featuring a flexible component system based on [Artemis Entity System Framework][artemis]. It features a GLSL-based renderer, a Lua scripting system with an animation API and physics system.

[artemis]: http://gamadu.com/artemis/
