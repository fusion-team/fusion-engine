#ifndef FUSION_FUSIONCONFIG_H
#define FUSION_FUSIONCONFIG_H

#include <climits>
#include <cstdint>

/* Define UINT<>_MAX if not already defined */
#ifndef UINT8_MAX
# define UINT8_MAX      (255)
# warning DEFINING UINT8_MAX
#endif

#ifndef UINT16_MAX
# define UINT16_MAX     (65535)
# warning DEFINING UINT16_MAX
#endif

#ifndef UINT32_MAX
# define UINT32_MAX     (4294967295U)
# warning DEFINING UINT32_MAX
#endif

#ifndef UINT64_MAX
# define UINT64_MAX     (__UINT64_C(18446744073709551615))
# warning DEFINING UINT64_MAX
#endif

/**
 * @brief    How many component types you expect to have.
 * @details  Increase this value if you expect to have more. Use powers of two.
 */
#ifndef MAX_COMPONENT_TYPES
#define MAX_COMPONENT_TYPES 64
#endif

/**
 * @brief	Initial number of entity ID to index mappings
 * @details	This numbers sets the initial array size for entity id to entity index mappings.
 */
#ifndef PREALOC_ENTITY2IDX_SIZE
#define PREALOC_ENTITY2IDX_SIZE  2048
#endif


#ifndef CHUNK_SIZE
#define CHUNK_SIZE 2048
#endif

#endif
