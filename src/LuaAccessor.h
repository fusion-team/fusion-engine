#ifndef FUSION_LUAACCESSOR_H
#define FUSION_LUAACCESSOR_H

#include <string>
#include <vector>

namespace Fusion
{
	class Lua;
	class LuaAccessor
	{
		public:
			LuaAccessor(const std::string & key, const LuaAccessor & parent);
			LuaAccessor(const std::string & key, Lua & lua);

			LuaAccessor operator[](const std::string & key);
			void operator=(int value);
			void operator=(float value);
			void operator=(unsigned value);
			void operator=(const std::string & value);
			void operator=(const char * value);
			void operator=(bool value);

			template<typename T> T as() { return T(); }
			template<typename T> bool is() { return false; }


		private:
			std::vector<std::string> path;
			std::string key;
			int stackPosition;
			Lua & lua;

		private:
			int push(bool pushSelf = true);
			void pop(bool popSelf = true);
	};

	template<> bool LuaAccessor::is<int>();
	template<> int LuaAccessor::as<int>();
}



#endif //FUSION_LUAACCESSOR_H
