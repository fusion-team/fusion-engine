#include "TextStream.h"
#include "DataStream.h"

#include <streambuf>
#include <sstream>

namespace Fusion
{
	TextStream TextStream::fromFile(const std::string & file, const char * modes)
	{
		return TextStream(file, modes);
	}

	TextStream TextStream::fromMemory(const void * mem, unsigned size)
	{
		return TextStream(mem, size);
	}

	TextStream TextStream::fromMemory(void * mem, unsigned size)
	{
		return TextStream(mem, size);
	}

	std::string TextStream::read(unsigned int size)
	{
		return std::string(reinterpret_cast<char *>(dataStream.read(size).data()), size);
	}

	std::string TextStream::readAll()
	{
		std::vector<unsigned char> v = dataStream.readAll();
		if (v.empty())
		{
			return std::string();
		}
		v.push_back(0); // string terminator
        return std::string(reinterpret_cast<char *>(v.data()));
	}

    char TextStream::readChar()
    {
        return dataStream.read(1)[0];
    }

	std::string TextStream::readLine()
	{
      std::stringbuf sb;

		for (;;)
		{
          unsigned char c = dataStream.read(1)[0];
			switch (c)
			{
				case '\n':
                return sb.str();
				case '\r':
                dataStream.read(1)[0]; //reading the \n
                return sb.str();
				default:
                  sb.sputc(c);
			}
		}
	}

	std::string TextStream::readWord()
	{
      std::stringbuf sb;

      for (;;)
      {
          unsigned char c = dataStream.read(1)[0];
          switch (c)
          {
          case ' ':
              if(sb.str().empty())
                  break;

          case '\n':
          case '\0':
              return sb.str();

          case '\r':
              dataStream.read(1)[0]; //reading the \n
              return sb.str();

          default:
              sb.sputc(c);
          }
      }

	}

	int TextStream::write(const std::string & str)
	{
        return dataStream.write(str);

	}

	int TextStream::seek(int pos)
	{
        return dataStream.seek(pos);
	}

	TextStream::TextStream(TextStream && other)
		: dataStream(std::move(other.dataStream))
	{
	}

	TextStream::TextStream(const std::string & file, const char * modes)
		: dataStream(std::move(DataStream::fromFile(file, modes)))
	{
	}

	TextStream::TextStream(void * mem, unsigned size)
		: dataStream(std::move(DataStream::fromMemory(mem, size)))
	{
	}

	TextStream::TextStream(const void * mem, unsigned size)
		: dataStream(std::move(DataStream::fromMemory(mem, size)))
	{
	}
}
