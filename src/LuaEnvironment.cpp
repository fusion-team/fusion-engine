#include "LuaEnvironment.h"
#include "lua.hpp"
#include "logtrace.h"

namespace Fusion {

	using LuaAccessor = LuaEnvironment::Accessor;

	LuaEnvironment::LuaEnvironment()
			: L(luaL_newstate())
	{
		luaL_openlibs(L);
	}

	void LuaEnvironment::execute(const std::string & file)
    {
        if (luaL_dofile(L, file.c_str()))
        {
             moderr("lua");
             trace.e("Lua error: ", file);
        }
	}

	LuaEnvironment::Accessor LuaEnvironment::operator[](const std::string & global)
	{
		lua_getglobal(L, global.c_str());
		return LuaAccessor(*this, global, static_cast<int16_t>(lua_absindex(L, -1)), 0);
	}

	LuaEnvironment::Accessor::Accessor(LuaEnvironment & env, const std::string & key, int16_t stack, int16_t parent)
			: env(env)
			, stack(stack)
			, parent(parent)
			, key(key)
	{ }

	LuaEnvironment::Accessor LuaEnvironment::Accessor::operator[](const std::string & field)
	{
		if (!lua_istable(env.L, stack)) {
			lua_remove(env.L, stack);
			int16_t table = parent;
			if (parent == 0) table = pushGlobalTable();
			env.push(key);
			lua_newtable(env.L);
			assign(table);
			if (parent == 0) {
				lua_getglobal(env.L, key.c_str());
			} else {
				lua_getfield(env.L, parent, key.c_str());
			}
			stack = static_cast<int16_t>(lua_absindex(env.L, -1));
		}
		lua_getfield(env.L, stack, field.c_str());
		return LuaAccessor(env, field, static_cast<int16_t>(lua_absindex(env.L, -1)), stack);
	}

		template<typename... Ts>
	LuaEnvironment::Accessor LuaEnvironment::Accessor::operator()(Ts &&... vs)
	{
		return LuaEnvironment::Accessor(env, "", 0, 0);
	}


    void LuaEnvironment::push(const int& v)
	{
		lua_pushinteger(L, v);
	}

    void LuaEnvironment::push(const double& v)
	{
		lua_pushnumber(L, v);
	}

    void LuaEnvironment::push(const bool& v)
	{
		lua_pushboolean(L, static_cast<int>(v));
	}

	void LuaEnvironment::push(const std::string & v)
	{
		lua_pushstring(L, v.c_str());
	}

	template<>
	bool LuaEnvironment::Accessor::is<double>() {
		return lua_isnumber(env.L, stack) == 1;
	}

	template<>
	bool LuaEnvironment::Accessor::is<int>() {
		return is<double>();
	}

	template<>
	bool LuaEnvironment::Accessor::is<std::string>() {
		return lua_isstring(env.L, stack) == 1;
	}

	template<>
	int LuaEnvironment::Accessor::as<int>(const int & def) {
		if (is<int>()) return (int) lua_tointeger(env.L, stack);
		return def;
	}

	template<>
	double LuaEnvironment::Accessor::as<double>(const double & def) {
		if (is<double>()) return (double) lua_tonumber(env.L, stack);
		return def;
	}

	template<>
	std::string LuaEnvironment::Accessor::as<std::string>(const std::string & def) {
		if (is<std::string>()) return std::string(lua_tostring(env.L, stack));
		return def;
	}

	int16_t LuaEnvironment::Accessor::pushGlobalTable() {
		lua_pushglobaltable(env.L);
		return static_cast<int16_t>(lua_absindex(env.L, -1));
	}
	void LuaEnvironment::Accessor::assign(int16_t table) {
		lua_settable(env.L, table);
		if (parent == 0) lua_pop(env.L, 1);
	}
}


