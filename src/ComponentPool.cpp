#include "ComponentPool.h"
#include <cassert>
#include "logtrace.h"

namespace Fusion
{
	ComponentPool::ComponentPool()
		: lastIndex((uint32_t) -1)
	{
	}

	ComponentPool::~ComponentPool()
	{
		// free every block
		for (ComponentStorage & storage : componentStorage)
		{
			// we only free using the first block
			if (storage.offset != 0 || storage.chunks == nullptr) continue;
			for (uint8_t *& chunk : *(storage.chunks))
				delete[] chunk;

			delete storage.chunks;
		}
	}

	void ComponentPool::zone(Component::Id id, size_t size)
	{
		modinfo("component-pool");
		ComponentStorage & storage = componentStorage[id];
		storage.blockSize = size;
		storage.offset = 0;
		storage.chunks = new std::vector<uint8_t *>;
		uint8_t * memory = new uint8_t[size * CHUNK_SIZE];
		trace("Component id", (int)id, "got memory address", (void*)memory);
		storage.chunks->push_back(memory);
		storage.cachedChunk = memory;
		storage.cachedChunkIndex = 0;
	}

	void ComponentPool::zone(const std::vector<std::pair<Component::Id, size_t>> & shared)
	{
		Component::Id id;
		size_t size;
		size_t blockSize = 0;
		std::vector<uint8_t *> * chunks = new std::vector<uint8_t *>;

		for (auto && p : shared)
		{
			// calculates sizes and offsets
			// also assigns the chunks pointer
			id = p.first;
			size = p.second;

			blockSize += size;
			ComponentStorage & storage = componentStorage[id];
			storage.chunks = chunks;
			storage.offset = blockSize - size;
			storage.cachedChunkIndex = 0;
		}


		// push the first memory block
		uint8_t * memory = new uint8_t[blockSize * CHUNK_SIZE];
		chunks->push_back(memory);

		// update blocksize and cache on everyone
		for (auto && p : shared)
		{
			componentStorage[p.first].blockSize = blockSize;
			componentStorage[p.first].cachedChunk = memory;
		}
	}

	void ComponentPool::stale(uint32_t index)
	{
		staleIndexes.push_back(index);
	}

	uint32_t ComponentPool::lease()
	{
		uint32_t index = 0;
		if (staleIndexes.empty())
		{
			lastIndex++;
			index = lastIndex;
		}
		else
		{
			index = staleIndexes.back();
			staleIndexes.pop_back();
		}
		return index;
	}

	size_t ComponentPool::count()
	{
		return lastIndex+1;
	}

	ComponentPool::ComponentStorage & ComponentPool::operator[](Component::Id id)
	{
		assert(id < MAX_COMPONENT_TYPES);
		return componentStorage[id];
	}

	uint8_t * ComponentPool::ComponentStorage::operator[](uint32_t index)
	{
		std::vector<uint8_t *> * c = chunks; // cache locally the ptr
		uint8_t * chunk = cachedChunk; // assume we're dealing with cached chunk
		size_t chunkIndex = index / CHUNK_SIZE;
		if (chunkIndex >= c->size()) // memory needed!
		{
			size_t newChunks = chunkIndex - c->size() + 1;
			while (newChunks--)
				c->push_back(new uint8_t[blockSize * CHUNK_SIZE]);
		}

		if (chunkIndex != cachedChunkIndex) // if its another chunk...
		{
			chunk = (*c)[chunkIndex];
			cachedChunk = chunk;
			cachedChunkIndex = chunkIndex;
		}

		size_t chunkOffset = (index % CHUNK_SIZE);
		return (chunk + (chunkOffset * blockSize)) + offset;
	}
}
