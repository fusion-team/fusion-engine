#include "DataStream.h"
#include "logtrace.h"
#include "SDL.h"


namespace Fusion
{

#ifdef __ANDROID__
    std::string DataStream::root = "";
#else
	std::string DataStream::root = "assets/";
#endif

	DataStream DataStream::fromFile(const std::string & file, const char * modes)
	{
		SDL_RWops * ops = SDL_RWFromFile((root + file).c_str(), modes);
		if (ops == nullptr)
		{
			moderr("datastream");
			trace("Could not load", file, "with modes", modes, ":", SDL_GetError());
		}
		return DataStream(ops);
	}

	DataStream DataStream::fromMemory(void * mem, unsigned size)
	{
		SDL_RWops * ops = SDL_RWFromMem(mem, size);
		if (ops == nullptr)
		{
			moderr("datastream");
			trace("Could not create DataStream from memory:", SDL_GetError());
		}
		return DataStream(ops);
	}

	DataStream DataStream::fromMemory(const void * mem, unsigned size)
	{
		SDL_RWops * ops = SDL_RWFromConstMem(mem, size);
		if (ops == nullptr)
		{
			moderr("datastream");
			trace("Could not create DataStream from memory:", SDL_GetError());
		}
		return DataStream(ops);
	}


	DataStream::DataStream(void * impl)
		: impl(impl)
		, atEOF(false)
	{ }

	DataStream::DataStream(DataStream && other)
		: impl(other.impl)
		, atEOF(false)
	{
		other.impl = nullptr;
	}

	DataStream::~DataStream()
	{
		if (impl != nullptr)
		{
			SDL_RWclose((SDL_RWops *)impl);
		}
	}

	std::vector<unsigned char> DataStream::read(unsigned size)
	{
		std::vector<unsigned char> r;
		if (!valid())
			return r;

		r.resize(size, 0);
		unsigned read = SDL_RWread((SDL_RWops *)impl, &r[0], 1, size);
		r.resize(read);
		if (read == 0) atEOF = true;
		return r;
	}

	std::vector<unsigned char> DataStream::readAll()
	{
		std::vector<unsigned char> r;
		if (!valid())
			return r;

		unsigned char c;
		unsigned read;

		do {
			read = SDL_RWread((SDL_RWops *)impl, &c, 1, sizeof(char));
			if (read == 0) atEOF = true;
			else r.push_back(c);
		} while (!eof());
		return r;
	}
	int DataStream::write(const std::string & data)
	{
		if (!valid()) return -1;
		return SDL_RWwrite((SDL_RWops *) impl, &data[0], 1, data.length());
	}

	int DataStream::write(const std::vector<unsigned char> & data)
	{
		if (!valid()) return -1;
		return SDL_RWwrite((SDL_RWops *) impl, &data[0], 1, data.size());
	}

	int DataStream::write(const void * data, unsigned size)
	{
		if (!valid()) return -1;
		return SDL_RWwrite((SDL_RWops *) impl, data, 1, size);
	}

	int DataStream::seek(int pos)
	{
		if (!valid()) return -1;
		return SDL_RWseek((SDL_RWops *)impl, pos, RW_SEEK_SET);
	}
}
