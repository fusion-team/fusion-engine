#include "Input.h"
#include "SDL.h"
#include "Processor.h"
#include "LuaEnvironment.h"

namespace Fusion {
	Input & Input::instance() {
		static Input input;
		return input;
	}

	Input::Input()
		: keyboardState(SDL_NUM_SCANCODES)
		, pointers((unsigned)SDL_GetNumTouchFingers(0) + 1) /* SDL has 5 buttons only but they may have more later */
		, inputQuitRequested(false)
	{
	}

	void Input::update(std::vector<Processor *> keyboardListeners, std::vector<Processor *> pointerListeners, std::vector<Processor *> wheelListeners)
	{
		/* cover keyboard */
		const uint8_t * isPressed = SDL_GetKeyboardState(NULL);
		bool changed = false;
		for (size_t k = 0; k < SDL_NUM_SCANCODES; k++)
		{
			changed = false;
			if (isPressed[k])
			{
				if (keyboardState[k] == Unpressed || keyboardState[k] == Released)
				{
					keyboardState[k] = Pressed;
					changed = true;
				}
				else if ((keyboardState[k] == Pressed))
				{
					keyboardState[k] = Pressing;
					changed = true;
				}

			}
			else
			{
				if (keyboardState[k] == Pressing || keyboardState[k] == Pressed)
				{
					keyboardState[k] = Released;
					changed = true;
				}
				else if (keyboardState[k] == Released)
				{
					keyboardState[k] = Unpressed;
					changed = true;
				}
			}
			if (changed)
			{
				for (Processor * p : keyboardListeners) p->onKeyboardInput(keyboardState[k], static_cast<KeyboardKey>(k));
			}
		}

		Math::ivec2 mouseAt;
		uint32_t mouseMask = SDL_GetMouseState(&mouseAt.x, &mouseAt.y);
		Pointer & p = pointers[0];

		/* TODO: pointerMovementInput callbacks? */
		/* TODO: normalize, how? ask renderer? */
		p.dx = mouseAt.x - p.x;
		p.dy = mouseAt.y - p.y;
		p.x = mouseAt.x;
		p.y = mouseAt.y;
		for (size_t b = 0; b < 5; b++)
		{
			PointerButton & pb = p[b];
			changed = false;
			if (mouseMask & SDL_BUTTON(b))
			{
				if (pb.state == Unpressed || pb.state == Released)
				{
					pb.state = Pressed;
					changed = true;
					pb.pressure = 1.0f;
				}
				else if (pb.state == Pressed)
				{
					pb.state = Pressing;
					changed = true;
					pb.pressure = 1.0f;
				}
			}
			else
			{
				if (pb.state == Pressing || pb.state == Pressed)
				{
					pb.state = Released;
					changed = true;
					pb.pressure = 1.0f;
				}
				else if (pointers[0][b].state == Released)
				{
					pb.state = Unpressed;
					changed = true;
					pb.pressure = 1.0f;
				}
			}
			if (changed)
			{
				for (Processor * proc : pointerListeners) proc->onPointerInput(p);
			}
		}

		wheelValues.x = wheelValues.y = 0;

		SDL_Event ev;

		inputQuitRequested = false;

		while (SDL_PollEvent(&ev))
		{
			/* calculate mouse wheel */
			switch (ev.type)
			{
				case SDL_MOUSEWHEEL:
					wheelValues.x = ev.wheel.x;
					wheelValues.y = ev.wheel.y;
					for (Processor * p : wheelListeners) p->onWheelInput(wheelValues);
				break;
				case SDL_QUIT:
					inputQuitRequested = true;
				break;

				default:
				break;
			}
		}
	}

    void Input::luaRegistry(LuaEnvironment &env)
    {
		/*
        env["KeyboardKey"]["Unknown"] = static_cast<int>(KeyboardKey::Unknown);
        env["KeyboardKey"]["A"] = static_cast<int>(KeyboardKey::A);
        env["KeyboardKey"]["B"] = static_cast<int>(KeyboardKey::B);
        env["KeyboardKey"]["C"] = static_cast<int>(KeyboardKey::C);
        env["KeyboardKey"]["D"] = static_cast<int>(KeyboardKey::D);
        env["KeyboardKey"]["E"] = static_cast<int>(KeyboardKey::E);
        env["KeyboardKey"]["F"] = static_cast<int>(KeyboardKey::F);
        env["KeyboardKey"]["G"] = static_cast<int>(KeyboardKey::G);
        env["KeyboardKey"]["H"] = static_cast<int>(KeyboardKey::H);
        env["KeyboardKey"]["I"] = static_cast<int>(KeyboardKey::I);
        env["KeyboardKey"]["J"] = static_cast<int>(KeyboardKey::J);
        env["KeyboardKey"]["K"] = static_cast<int>(KeyboardKey::K);
        env["KeyboardKey"]["L"] = static_cast<int>(KeyboardKey::L);
        env["KeyboardKey"]["M"] = static_cast<int>(KeyboardKey::M);
        env["KeyboardKey"]["N"] = static_cast<int>(KeyboardKey::N);
        env["KeyboardKey"]["O"] = static_cast<int>(KeyboardKey::O);
        env["KeyboardKey"]["P"] = static_cast<int>(KeyboardKey::P);
        env["KeyboardKey"]["Q"] = static_cast<int>(KeyboardKey::Q);
        env["KeyboardKey"]["R"] = static_cast<int>(KeyboardKey::R);
        env["KeyboardKey"]["S"] = static_cast<int>(KeyboardKey::S);
        env["KeyboardKey"]["T"] = static_cast<int>(KeyboardKey::T);
        env["KeyboardKey"]["U"] = static_cast<int>(KeyboardKey::U);
        env["KeyboardKey"]["V"] = static_cast<int>(KeyboardKey::V);
        env["KeyboardKey"]["W"] = static_cast<int>(KeyboardKey::W);
        env["KeyboardKey"]["X"] = static_cast<int>(KeyboardKey::X);
        env["KeyboardKey"]["Y"] = static_cast<int>(KeyboardKey::Y);
        env["KeyboardKey"]["Z"] = static_cast<int>(KeyboardKey::Z);
        env["KeyboardKey"]["Number1"] = static_cast<int>(KeyboardKey::Number1);
        env["KeyboardKey"]["Number2"] = static_cast<int>(KeyboardKey::Number2);
        env["KeyboardKey"]["Number3"] = static_cast<int>(KeyboardKey::Number3);
        env["KeyboardKey"]["Number4"] = static_cast<int>(KeyboardKey::Number4);
        env["KeyboardKey"]["Number5"] = static_cast<int>(KeyboardKey::Number5);
        env["KeyboardKey"]["Number6"] = static_cast<int>(KeyboardKey::Number6);
        env["KeyboardKey"]["Number7"] = static_cast<int>(KeyboardKey::Number7);
        env["KeyboardKey"]["Number8"] = static_cast<int>(KeyboardKey::Number8);
        env["KeyboardKey"]["Number9"] = static_cast<int>(KeyboardKey::Number9);
        env["KeyboardKey"]["Number0"] = static_cast<int>(KeyboardKey::Number0);
        env["KeyboardKey"]["Return"] = static_cast<int>(KeyboardKey::Return);
        env["KeyboardKey"]["Escape"] = static_cast<int>(KeyboardKey::Escape);
        env["KeyboardKey"]["Backspace"] = static_cast<int>(KeyboardKey::Backspace);
        env["KeyboardKey"]["Tab"] = static_cast<int>(KeyboardKey::Tab);
        env["KeyboardKey"]["Space"] = static_cast<int>(KeyboardKey::Space);
        env["KeyboardKey"]["Minus"] = static_cast<int>(KeyboardKey::Minus);
        env["KeyboardKey"]["Equals"] = static_cast<int>(KeyboardKey::Equals);
        env["KeyboardKey"]["LeftBracket"] = static_cast<int>(KeyboardKey::LeftBracket);
        env["KeyboardKey"]["RightBracket"] = static_cast<int>(KeyboardKey::RightBracket);
        env["KeyboardKey"]["Backslash"] = static_cast<int>(KeyboardKey::Backslash);
        env["KeyboardKey"]["Nonushash"] = static_cast<int>(KeyboardKey::Nonushash);
        env["KeyboardKey"]["Semicolon"] = static_cast<int>(KeyboardKey::Semicolon);
        env["KeyboardKey"]["Apostrophe"] = static_cast<int>(KeyboardKey::Apostrophe);
        env["KeyboardKey"]["Grave"] = static_cast<int>(KeyboardKey::Grave);
        env["KeyboardKey"]["Comma"] = static_cast<int>(KeyboardKey::Comma);
        env["KeyboardKey"]["Period"] = static_cast<int>(KeyboardKey::Period);
        env["KeyboardKey"]["Slash"] = static_cast<int>(KeyboardKey::Slash);
        env["KeyboardKey"]["CapsLock"] = static_cast<int>(KeyboardKey::CapsLock);
        env["KeyboardKey"]["F1"] = static_cast<int>(KeyboardKey::F1);
        env["KeyboardKey"]["F2"] = static_cast<int>(KeyboardKey::F2);
        env["KeyboardKey"]["F3"] = static_cast<int>(KeyboardKey::F3);
        env["KeyboardKey"]["F4"] = static_cast<int>(KeyboardKey::F4);
        env["KeyboardKey"]["F5"] = static_cast<int>(KeyboardKey::F5);
        env["KeyboardKey"]["F6"] = static_cast<int>(KeyboardKey::F6);
        env["KeyboardKey"]["F7"] = static_cast<int>(KeyboardKey::F7);
        env["KeyboardKey"]["F8"] = static_cast<int>(KeyboardKey::F8);
        env["KeyboardKey"]["F9"] = static_cast<int>(KeyboardKey::F9);
        env["KeyboardKey"]["F10"] = static_cast<int>(KeyboardKey::F10);
        env["KeyboardKey"]["F11"] = static_cast<int>(KeyboardKey::F11);
        env["KeyboardKey"]["F12"] = static_cast<int>(KeyboardKey::F12);
        env["KeyboardKey"]["PrintScreen"] = static_cast<int>(KeyboardKey::PrintScreen);
        env["KeyboardKey"]["ScrollLock"] = static_cast<int>(KeyboardKey::ScrollLock);
        env["KeyboardKey"]["Pause"] = static_cast<int>(KeyboardKey::Pause);
        env["KeyboardKey"]["Insert"] = static_cast<int>(KeyboardKey::Insert);
        env["KeyboardKey"]["Home"] = static_cast<int>(KeyboardKey::Home);
        env["KeyboardKey"]["PageUp"] = static_cast<int>(KeyboardKey::PageUp);
        env["KeyboardKey"]["Delete"] = static_cast<int>(KeyboardKey::Delete);
        env["KeyboardKey"]["End"] = static_cast<int>(KeyboardKey::End);
        env["KeyboardKey"]["PageDown"] = static_cast<int>(KeyboardKey::PageDown);
        env["KeyboardKey"]["Right"] = static_cast<int>(KeyboardKey::Right);
        env["KeyboardKey"]["Left"] = static_cast<int>(KeyboardKey::Left);
        env["KeyboardKey"]["Down"] = static_cast<int>(KeyboardKey::Down);
        env["KeyboardKey"]["Up"] = static_cast<int>(KeyboardKey::Up);
        env["KeyboardKey"]["NumLockClear"] = static_cast<int>(KeyboardKey::NumLockClear);
        env["KeyboardKey"]["KP_Divide"] = static_cast<int>(KeyboardKey::KP_Divide);
        env["KeyboardKey"]["KP_Multiply"] = static_cast<int>(KeyboardKey::KP_Multiply);
        env["KeyboardKey"]["KP_Minus"] = static_cast<int>(KeyboardKey::KP_Minus);
        env["KeyboardKey"]["KP_Plus"] = static_cast<int>(KeyboardKey::KP_Plus);
        env["KeyboardKey"]["KP_Enter"] = static_cast<int>(KeyboardKey::KP_Enter);
        env["KeyboardKey"]["KP_1"] = static_cast<int>(KeyboardKey::KP_1);
        env["KeyboardKey"]["KP_2"] = static_cast<int>(KeyboardKey::KP_2);
        env["KeyboardKey"]["KP_3"] = static_cast<int>(KeyboardKey::KP_3);
        env["KeyboardKey"]["KP_4"] = static_cast<int>(KeyboardKey::KP_4);
        env["KeyboardKey"]["KP_5"] = static_cast<int>(KeyboardKey::KP_5);
        env["KeyboardKey"]["KP_6"] = static_cast<int>(KeyboardKey::KP_6);
        env["KeyboardKey"]["KP_7"] = static_cast<int>(KeyboardKey::KP_7);
        env["KeyboardKey"]["KP_8"] = static_cast<int>(KeyboardKey::KP_8);
        env["KeyboardKey"]["KP_9"] = static_cast<int>(KeyboardKey::KP_9);
        env["KeyboardKey"]["KP_0"] = static_cast<int>(KeyboardKey::KP_0);
        env["KeyboardKey"]["KP_Period"] = static_cast<int>(KeyboardKey::KP_Period);
        env["KeyboardKey"]["NonusBackslash"] = static_cast<int>(KeyboardKey::NonusBackslash);
        env["KeyboardKey"]["Application"] = static_cast<int>(KeyboardKey::Application);
        env["KeyboardKey"]["Power"] = static_cast<int>(KeyboardKey::Power);
        env["KeyboardKey"]["KP_Equals"] = static_cast<int>(KeyboardKey::KP_Equals);
        env["KeyboardKey"]["F13"] = static_cast<int>(KeyboardKey::F13);
        env["KeyboardKey"]["F14"] = static_cast<int>(KeyboardKey::F14);
        env["KeyboardKey"]["F15"] = static_cast<int>(KeyboardKey::F15);
        env["KeyboardKey"]["F16"] = static_cast<int>(KeyboardKey::F16);
        env["KeyboardKey"]["F17"] = static_cast<int>(KeyboardKey::F17);
        env["KeyboardKey"]["F18"] = static_cast<int>(KeyboardKey::F18);
        env["KeyboardKey"]["F19"] = static_cast<int>(KeyboardKey::F19);
        env["KeyboardKey"]["F20"] = static_cast<int>(KeyboardKey::F20);
        env["KeyboardKey"]["F21"] = static_cast<int>(KeyboardKey::F21);
        env["KeyboardKey"]["F22"] = static_cast<int>(KeyboardKey::F22);
        env["KeyboardKey"]["F23"] = static_cast<int>(KeyboardKey::F23);
        env["KeyboardKey"]["F24"] = static_cast<int>(KeyboardKey::F24);
        env["KeyboardKey"]["Execute"] = static_cast<int>(KeyboardKey::Execute);
        env["KeyboardKey"]["Help"] = static_cast<int>(KeyboardKey::Help);
        env["KeyboardKey"]["Menu"] = static_cast<int>(KeyboardKey::Menu);
        env["KeyboardKey"]["Select"] = static_cast<int>(KeyboardKey::Select);
        env["KeyboardKey"]["Stop"] = static_cast<int>(KeyboardKey::Stop);
        env["KeyboardKey"]["Again"] = static_cast<int>(KeyboardKey::Again);
        env["KeyboardKey"]["Undo"] = static_cast<int>(KeyboardKey::Undo);
        env["KeyboardKey"]["Cut"] = static_cast<int>(KeyboardKey::Cut);
        env["KeyboardKey"]["Copy"] = static_cast<int>(KeyboardKey::Copy);
        env["KeyboardKey"]["Paste"] = static_cast<int>(KeyboardKey::Paste);
        env["KeyboardKey"]["Find"] = static_cast<int>(KeyboardKey::Find);
        env["KeyboardKey"]["Mute"] = static_cast<int>(KeyboardKey::Mute);
        env["KeyboardKey"]["VolumeUp"] = static_cast<int>(KeyboardKey::VolumeUp);
        env["KeyboardKey"]["VolumeDown"] = static_cast<int>(KeyboardKey::VolumeDown);
        env["KeyboardKey"]["KP_Comma"] = static_cast<int>(KeyboardKey::KP_Comma);
        env["KeyboardKey"]["KP_EqualsAs400"] = static_cast<int>(KeyboardKey::KP_EqualsAs400);
        env["KeyboardKey"]["International1"] = static_cast<int>(KeyboardKey::International1);
        env["KeyboardKey"]["International2"] = static_cast<int>(KeyboardKey::International2);
        env["KeyboardKey"]["International3"] = static_cast<int>(KeyboardKey::International3);
        env["KeyboardKey"]["International4"] = static_cast<int>(KeyboardKey::International4);
        env["KeyboardKey"]["International5"] = static_cast<int>(KeyboardKey::International5);
        env["KeyboardKey"]["International6"] = static_cast<int>(KeyboardKey::International6);
        env["KeyboardKey"]["International7"] = static_cast<int>(KeyboardKey::International7);
        env["KeyboardKey"]["International8"] = static_cast<int>(KeyboardKey::International8);
        env["KeyboardKey"]["International9"] = static_cast<int>(KeyboardKey::International9);
        env["KeyboardKey"]["Lang1"] = static_cast<int>(KeyboardKey::Lang1);
        env["KeyboardKey"]["Lang2"] = static_cast<int>(KeyboardKey::Lang2);
        env["KeyboardKey"]["Lang3"] = static_cast<int>(KeyboardKey::Lang3);
        env["KeyboardKey"]["Lang4"] = static_cast<int>(KeyboardKey::Lang4);
        env["KeyboardKey"]["Lang5"] = static_cast<int>(KeyboardKey::Lang5);
        env["KeyboardKey"]["Lang6"] = static_cast<int>(KeyboardKey::Lang6);
        env["KeyboardKey"]["Lang7"] = static_cast<int>(KeyboardKey::Lang7);
        env["KeyboardKey"]["Lang8"] = static_cast<int>(KeyboardKey::Lang8);
        env["KeyboardKey"]["Lang9"] = static_cast<int>(KeyboardKey::Lang9);
        env["KeyboardKey"]["AltErase"] = static_cast<int>(KeyboardKey::AltErase);
        env["KeyboardKey"]["SysReq"] = static_cast<int>(KeyboardKey::SysReq);
        env["KeyboardKey"]["Cancel"] = static_cast<int>(KeyboardKey::Cancel);
        env["KeyboardKey"]["Clear"] = static_cast<int>(KeyboardKey::Clear);
        env["KeyboardKey"]["Prior"] = static_cast<int>(KeyboardKey::Prior);
        env["KeyboardKey"]["Return2"] = static_cast<int>(KeyboardKey::Return2);
        env["KeyboardKey"]["Separator"] = static_cast<int>(KeyboardKey::Separator);
        env["KeyboardKey"]["Out"] = static_cast<int>(KeyboardKey::Out);
        env["KeyboardKey"]["Oper"] = static_cast<int>(KeyboardKey::Oper);
        env["KeyboardKey"]["ClearAgain"] = static_cast<int>(KeyboardKey::ClearAgain);
        env["KeyboardKey"]["CrSel"] = static_cast<int>(KeyboardKey::CrSel);
        env["KeyboardKey"]["ExSel"] = static_cast<int>(KeyboardKey::ExSel);
        env["KeyboardKey"]["KP_00"] = static_cast<int>(KeyboardKey::KP_00);
        env["KeyboardKey"]["KP_000"] = static_cast<int>(KeyboardKey::KP_000);
        env["KeyboardKey"]["ThousandsSeparator"] = static_cast<int>(KeyboardKey::ThousandsSeparator);
        env["KeyboardKey"]["DecimalSeparator"] = static_cast<int>(KeyboardKey::DecimalSeparator);
        env["KeyboardKey"]["CurrencyUnit"] = static_cast<int>(KeyboardKey::CurrencyUnit);
        env["KeyboardKey"]["CurrencySubUnit"] = static_cast<int>(KeyboardKey::CurrencySubUnit);
        env["KeyboardKey"]["KP_LeftParen"] = static_cast<int>(KeyboardKey::KP_LeftParen);
        env["KeyboardKey"]["KP_RightParen"] = static_cast<int>(KeyboardKey::KP_RightParen);
        env["KeyboardKey"]["KP_LeftBrace"] = static_cast<int>(KeyboardKey::KP_LeftBrace);
        env["KeyboardKey"]["KP_RightBrace"] = static_cast<int>(KeyboardKey::KP_RightBrace);
        env["KeyboardKey"]["KP_Tab"] = static_cast<int>(KeyboardKey::KP_Tab);
        env["KeyboardKey"]["KP_Backspace"] = static_cast<int>(KeyboardKey::KP_Backspace);
        env["KeyboardKey"]["KP_A"] = static_cast<int>(KeyboardKey::KP_A);
        env["KeyboardKey"]["KP_B"] = static_cast<int>(KeyboardKey::KP_B);
        env["KeyboardKey"]["KP_C"] = static_cast<int>(KeyboardKey::KP_C);
        env["KeyboardKey"]["KP_D"] = static_cast<int>(KeyboardKey::KP_D);
        env["KeyboardKey"]["KP_E"] = static_cast<int>(KeyboardKey::KP_E);
        env["KeyboardKey"]["KP_F"] = static_cast<int>(KeyboardKey::KP_F);
        env["KeyboardKey"]["KP_Xor"] = static_cast<int>(KeyboardKey::KP_Xor);
        env["KeyboardKey"]["KP_Power"] = static_cast<int>(KeyboardKey::KP_Power);
        env["KeyboardKey"]["KP_Percent"] = static_cast<int>(KeyboardKey::KP_Percent);
        env["KeyboardKey"]["KP_Less"] = static_cast<int>(KeyboardKey::KP_Less);
        env["KeyboardKey"]["KP_Greater"] = static_cast<int>(KeyboardKey::KP_Greater);
        env["KeyboardKey"]["KP_Ampersand"] = static_cast<int>(KeyboardKey::KP_Ampersand);
        env["KeyboardKey"]["KP_DblAmpersand"] = static_cast<int>(KeyboardKey::KP_DblAmpersand);
        env["KeyboardKey"]["KP_VerticalBar"] = static_cast<int>(KeyboardKey::KP_VerticalBar);
        env["KeyboardKey"]["KP_DblVerticalBar"] = static_cast<int>(KeyboardKey::KP_DblVerticalBar);
        env["KeyboardKey"]["KP_Colon"] = static_cast<int>(KeyboardKey::KP_Colon);
        env["KeyboardKey"]["KP_Hash"] = static_cast<int>(KeyboardKey::KP_Hash);
        env["KeyboardKey"]["KP_Space"] = static_cast<int>(KeyboardKey::KP_Space);
        env["KeyboardKey"]["KP_At"] = static_cast<int>(KeyboardKey::KP_At);
        env["KeyboardKey"]["KP_Exclam"] = static_cast<int>(KeyboardKey::KP_Exclam);
        env["KeyboardKey"]["KP_MemStore"] = static_cast<int>(KeyboardKey::KP_MemStore);
        env["KeyboardKey"]["KP_MemRecall"] = static_cast<int>(KeyboardKey::KP_MemRecall);
        env["KeyboardKey"]["KP_MemClear"] = static_cast<int>(KeyboardKey::KP_MemClear);
        env["KeyboardKey"]["KP_MemAdd"] = static_cast<int>(KeyboardKey::KP_MemAdd);
        env["KeyboardKey"]["KP_MemSubtract"] = static_cast<int>(KeyboardKey::KP_MemSubtract);
        env["KeyboardKey"]["KP_MemMultiply"] = static_cast<int>(KeyboardKey::KP_MemMultiply);
        env["KeyboardKey"]["KP_MemDivide"] = static_cast<int>(KeyboardKey::KP_MemDivide);
        env["KeyboardKey"]["KP_PlusMinus"] = static_cast<int>(KeyboardKey::KP_PlusMinus);
        env["KeyboardKey"]["KP_Clear"] = static_cast<int>(KeyboardKey::KP_Clear);
        env["KeyboardKey"]["KP_ClearEntry"] = static_cast<int>(KeyboardKey::KP_ClearEntry);
        env["KeyboardKey"]["KP_Binary"] = static_cast<int>(KeyboardKey::KP_Binary);
        env["KeyboardKey"]["KP_Octal"] = static_cast<int>(KeyboardKey::KP_Octal);
        env["KeyboardKey"]["KP_Decimal"] = static_cast<int>(KeyboardKey::KP_Decimal);
        env["KeyboardKey"]["KP_Hexadecimal"] = static_cast<int>(KeyboardKey::KP_Hexadecimal);
        env["KeyboardKey"]["LCtrl"] = static_cast<int>(KeyboardKey::LCtrl);
        env["KeyboardKey"]["LShift"] = static_cast<int>(KeyboardKey::LShift);
        env["KeyboardKey"]["LAlt"] = static_cast<int>(KeyboardKey::LAlt);
        env["KeyboardKey"]["LGui"] = static_cast<int>(KeyboardKey::LGui);
        env["KeyboardKey"]["RCtrl"] = static_cast<int>(KeyboardKey::RCtrl);
        env["KeyboardKey"]["RShift"] = static_cast<int>(KeyboardKey::RShift);
        env["KeyboardKey"]["RAlt"] = static_cast<int>(KeyboardKey::RAlt);
        env["KeyboardKey"]["RGui"] = static_cast<int>(KeyboardKey::RGui);
        env["KeyboardKey"]["Mode"] = static_cast<int>(KeyboardKey::Mode);
        env["KeyboardKey"]["AudioNext"] = static_cast<int>(KeyboardKey::AudioNext);
        env["KeyboardKey"]["AudioPrev"] = static_cast<int>(KeyboardKey::AudioPrev);
        env["KeyboardKey"]["AudioStop"] = static_cast<int>(KeyboardKey::AudioStop);
        env["KeyboardKey"]["AudioPlay"] = static_cast<int>(KeyboardKey::AudioPlay);
        env["KeyboardKey"]["AudioMute"] = static_cast<int>(KeyboardKey::AudioMute);
        env["KeyboardKey"]["MediaSelect"] = static_cast<int>(KeyboardKey::MediaSelect);
        env["KeyboardKey"]["WWW"] = static_cast<int>(KeyboardKey::WWW);
        env["KeyboardKey"]["Mail"] = static_cast<int>(KeyboardKey::Mail);
        env["KeyboardKey"]["Calculator"] = static_cast<int>(KeyboardKey::Calculator);
        env["KeyboardKey"]["Computer"] = static_cast<int>(KeyboardKey::Computer);
        env["KeyboardKey"]["AC_Search"] = static_cast<int>(KeyboardKey::AC_Search);
        env["KeyboardKey"]["AC_Home"] = static_cast<int>(KeyboardKey::AC_Home);
        env["KeyboardKey"]["AC_Back"] = static_cast<int>(KeyboardKey::AC_Back);
        env["KeyboardKey"]["AC_Forward"] = static_cast<int>(KeyboardKey::AC_Forward);
        env["KeyboardKey"]["AC_Stop"] = static_cast<int>(KeyboardKey::AC_Stop);
        env["KeyboardKey"]["AC_Refresh"] = static_cast<int>(KeyboardKey::AC_Refresh);
        env["KeyboardKey"]["AC_Bookmarks"] = static_cast<int>(KeyboardKey::AC_Bookmarks);
        env["KeyboardKey"]["BrightnessDown"] = static_cast<int>(KeyboardKey::BrightnessDown);
        env["KeyboardKey"]["BrightnessUp"] = static_cast<int>(KeyboardKey::BrightnessUp);
        env["KeyboardKey"]["DisplaySwitch"] = static_cast<int>(KeyboardKey::DisplaySwitch);
        env["KeyboardKey"]["KBDillumToggle"] = static_cast<int>(KeyboardKey::KBDillumToggle);
        env["KeyboardKey"]["KBDillumDown"] = static_cast<int>(KeyboardKey::KBDillumDown);
        env["KeyboardKey"]["KBDillumUp"] = static_cast<int>(KeyboardKey::KBDillumUp);
        env["KeyboardKey"]["Eject"] = static_cast<int>(KeyboardKey::Eject);
        env["KeyboardKey"]["Sleep"] = static_cast<int>(KeyboardKey::Sleep);
        env["KeyboardKey"]["App1"] = static_cast<int>(KeyboardKey::App1);
        env["KeyboardKey"]["App2"] = static_cast<int>(KeyboardKey::App2);
        */
    }
}
