#include "Image.h"
#include <cstddef>
namespace Fusion
{
	Image::Image(const Image & other)
		: bitmap{other.bitmap}
		, w{other.w}
		, h{other.h}
	{ }

	Image::Image(Image && other)
		: bitmap{std::move(other.bitmap)}
		, w{other.w}
		, h{other.h}
	{ }

	Image::Image(const unsigned char * data, unsigned width, unsigned height)
		: bitmap(width*height*4, 0)
		, w{width}
		, h{height}
	{
		if (w == 0 || h == 0)
			return;
		std::copy(data, data+(w*h*4), bitmap.begin());
	}

	Image::~Image()
	{ }
}
