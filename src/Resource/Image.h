#ifndef FUSION_IMAGE_H
#define FUSION_IMAGE_H

#include <vector>

namespace Fusion
{
	class Image
	{
		public:
			Image(Image && other);
			Image(const Image & other);
			Image(const unsigned char * data = nullptr, unsigned width = 0, unsigned height = 0);

			std::vector<unsigned char> & pixels() { return bitmap; }
			unsigned width() { return w; }
			unsigned height() { return h; }

			~Image();
			Image() = delete;

		private:
			std::vector<unsigned char> bitmap;
			unsigned w;
			unsigned h;
	};
}

#endif
