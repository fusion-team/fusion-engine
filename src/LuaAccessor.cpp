#include "LuaAccessor.h"
#include "Lua.h"
#include "logtrace.h"
#include <ostream>
#include <vector>

template<typename T>
static inline std::ostream & operator<<(std::ostream & os, const std::vector<T> & values)
{
	unsigned i = 0, size = values.size();
	if (size == 0) return os;
	for (i = 0; i < size-1; i++)
		os << values[i] << '.';
	os << values[i];
	return os;
}

namespace Fusion
{
	LuaAccessor::LuaAccessor(const std::string & key, const LuaAccessor & parent)
		: path(parent.path)
		, key(key)
		, stackPosition(-1)
		, lua(parent.lua)
	{
		if (!parent.key.empty())
			path.push_back(parent.key);
	}

	LuaAccessor::LuaAccessor(const std::string & key, Lua & lua)
		: path()
		, key(key)
		, stackPosition(-1)
		, lua(lua)
	{ }

	LuaAccessor LuaAccessor::operator[](const std::string & key)
	{
		return LuaAccessor(key, *this);
	}

	void LuaAccessor::operator=(int value)
	{
		push(false);
		lua.setField(-2, key, value);
		pop(false);
	}

	void LuaAccessor::operator=(float value)
	{
		push(false);
		lua.setField(-2, key, value);
		pop(false);
	}

	void LuaAccessor::operator=(unsigned value)
	{
		push(false);
		lua.setField(-2, key, value);
		pop(false);
	}

	void LuaAccessor::operator=(const std::string & value)
	{
		operator=(value.c_str());
	}

	void LuaAccessor::operator=(const char * value)
	{
		push(false);
		lua.setField(-2, key, value);
		pop(false);
	}

	void LuaAccessor::operator=(bool value)
	{
		push(false);
		lua.setField(-2, key, value);
		pop(false);
	}

	template<>
	bool LuaAccessor::is<int>()
	{
		push();
		bool isInt = lua.is<int>();
		pop();
		return isInt;
	}

	template<>
	int LuaAccessor::as<int>()
	{
		int value = 0;
		push();
		if (lua.is<int>())
			value = lua.as<int>();
		else {
			moderr("lua-accessor");
			trace("Cannot convert", path, "[", key, "]", "to int");
		}
		pop();
		return value;
	}

	int LuaAccessor::push(bool pushSelf)
	{
		modinfo("lua-accessor");
		if (path.size() == 0) {
			lua.global(key.c_str());
		}
		else
		{
			for (unsigned i = 0; i < path.size(); i++)
			{
				trace.i("Accessing", path[i]);
				if (i == 0) {
					lua.global(path[i].c_str());
				} else
				{
					if (!lua.is<Lua::Table>())
					{
						trace.e("Cannot index", path[i], "part of path", path);
						return 0;
					}
					lua.field(-1, path[i].c_str());
				}
			}
			trace.i("Finished pathwalk, accessing", key);
			if (pushSelf)
			{
				if (!lua.is<Lua::Table>())
				{
					trace("Cannot index", key, "in", path, "because it is not a table!");
					return 0;
				}
				lua.field(-1, key.c_str());
			}
		}
		return lua.top();
	}

	void LuaAccessor::pop(bool popSelf)
	{
		lua.pop(path.size() + popSelf? 1 : 0);
	}
}
