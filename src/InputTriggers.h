#ifndef FUSION_KEYS_H
#define FUSION_KEYS_H

namespace Fusion {

	enum class MouseButton
	{
		Left = 1,
		Middle,
		Right,
		X1,
		X2,
	};

	enum class MouseWheel {
		Horizontal = 0,
		Vertical = 1
	};

	/* shamelessly copied transformed from SDL */
	enum class KeyboardKey
	{
		Unknown = 0,
		A = 4,
		B = 5,
		C = 6,
		D = 7,
		E = 8,
		F = 9,
		G = 10,
		H = 11,
		I = 12,
		J = 13,
		K = 14,
		L = 15,
		M = 16,
		N = 17,
		O = 18,
		P = 19,
		Q = 20,
		R = 21,
		S = 22,
		T = 23,
		U = 24,
		V = 25,
		W = 26,
		X = 27,
		Y = 28,
		Z = 29,

		Number1 = 30,
		Number2 = 31,
		Number3 = 32,
		Number4 = 33,
		Number5 = 34,
		Number6 = 35,
		Number7 = 36,
		Number8 = 37,
		Number9 = 38,
		Number0 = 39,

		Return = 40,
		Escape = 41,
		Backspace = 42,
		Tab = 43,
		Space = 44,

		Minus = 45,
		Equals = 46,
		LeftBracket = 47,
		RightBracket = 48,
		Backslash = 49, /**< located at the lower left of the return
						  *   key on iso keyboards and at the right end
						  *   of the qwerty row on ansi keyboards.
						  *   produces reverse solidus (backslash) and
						  *   vertical line in a us layout, reverse
						  *   solidus and vertical line in a uk mac
						  *   layout, number sign and tilde in a uk
						  *   windows layout, dollar sign and pound sign
						  *   in a swiss german layout, number sign and
						  *   apostrophe in a german layout, grave
						  *   accent and pound sign in a french mac
						  *   layout, and asterisk and micro sign in a
						  *   french windows layout.
						  */
		Nonushash = 50, /**< iso usb keyboards actually use this code
						  *   instead of 49 for the same key, but all
						  *   oses i've seen treat the two codes
						  *   identically. so, as an implementor, unless
						  *   your keyboard generates both of those
						  *   codes and your os treats them differently,
						  *   you should generate backslash
						  *   instead of this code. as a user, you
						  *   should not rely on this code because sdl
						  *   will never generate it with most (all?)
						  *   keyboards.
						  */
		Semicolon = 51,
		Apostrophe = 52,
		Grave = 53, /**< located in the top left corner (on both ansi
					  *   and iso keyboards). produces grave accent and
					  *   tilde in a us windows layout and in us and uk
					  *   mac layouts on ansi keyboards, grave accent
					  *   and not sign in a uk windows layout, section
					  *   sign and plus-minus sign in us and uk mac
					  *   layouts on iso keyboards, section sign and
					  *   degree sign in a swiss german layout (mac:
					  *   only on iso keyboards), circumflex accent and
					  *   degree sign in a german layout (mac: only on
					  *   iso keyboards), superscript two and tilde in a
					  *   french windows layout, commercial at and
					  *   number sign in a french mac layout on iso
					  *   keyboards, and less-than sign and greater-than
					  *   sign in a swiss german, german, or french mac
					  *   layout on ansi keyboards.
					  */
		Comma = 54,
		Period = 55,
		Slash = 56,

		CapsLock = 57,

		F1 = 58,
		F2 = 59,
		F3 = 60,
		F4 = 61,
		F5 = 62,
		F6 = 63,
		F7 = 64,
		F8 = 65,
		F9 = 66,
		F10 = 67,
		F11 = 68,
		F12 = 69,

		PrintScreen = 70,
		ScrollLock = 71,
		Pause = 72,
		Insert = 73, /**< insert on pc, help on some mac keyboards (but Does send code 73, not 117) */
		Home = 74,
		PageUp = 75,
		Delete = 76,
		End = 77,
		PageDown = 78,
		Right = 79,
		Left = 80,
		Down = 81,
		Up = 82,

		NumLockClear = 83, /**< num lock on pc, clear on mac keyboards */
		KP_Divide = 84,
		KP_Multiply = 85,
		KP_Minus = 86,
		KP_Plus = 87,
		KP_Enter = 88,
		KP_1 = 89,
		KP_2 = 90,
		KP_3 = 91,
		KP_4 = 92,
		KP_5 = 93,
		KP_6 = 94,
		KP_7 = 95,
		KP_8 = 96,
		KP_9 = 97,
		KP_0 = 98,
		KP_Period = 99,

		NonusBackslash = 100, /**< this is the additional key that iso
						   *   keyboards have over ansi ones,
						   *   located between left shift and y.
						   *   produces grave accent and tilde in a
						   *   us or uk mac layout, reverse solidus
						   *   (backslash) and vertical line in a
						   *   us or uk windows layout, and
						   *   less-than sign and greater-than sign
						   *   in a swiss german, german, or french
						   *   layout. */
		Application = 101, /**< windows contextual menu, compose */
		Power = 102, /**< the usb document says this is a status flag,
					  *   not a physical key - but some mac keyboards
					  *   do have a power key. */
		KP_Equals = 103,
		F13 = 104,
		F14 = 105,
		F15 = 106,
		F16 = 107,
		F17 = 108,
		F18 = 109,
		F19 = 110,
		F20 = 111,
		F21 = 112,
		F22 = 113,
		F23 = 114,
		F24 = 115,
		Execute = 116,
		Help = 117,
		Menu = 118,
		Select = 119,
		Stop = 120,
		Again = 121,   /**< redo */
		Undo = 122,
		Cut = 123,
		Copy = 124,
		Paste = 125,
		Find = 126,
		Mute = 127,
		VolumeUp = 128,
		VolumeDown = 129,
		/* not sure whether there's a reason to enable these */
		/*     lockingcapslock = 130,  */
		/*     lockingnumlock = 131, */
		/*     lockingscrolllock = 132, */
		KP_Comma = 133,
		KP_EqualsAs400 = 134,

		International1 = 135, /**< used on asian keyboards, see Footnotes in usb doc */
		International2 = 136,
		International3 = 137, /**< yen */
		International4 = 138,
		International5 = 139,
		International6 = 140,
		International7 = 141,
		International8 = 142,
		International9 = 143,
		Lang1 = 144, /**< hangul/english toggle */
		Lang2 = 145, /**< hanja conversion */
		Lang3 = 146, /**< katakana */
		Lang4 = 147, /**< hiragana */
		Lang5 = 148, /**< zenkaku/hankaku */
		Lang6 = 149, /**< reserved */
		Lang7 = 150, /**< reserved */
		Lang8 = 151, /**< reserved */
		Lang9 = 152, /**< reserved */

		AltErase = 153, /**< erase-eaze */
		SysReq = 154,
		Cancel = 155,
		Clear = 156,
		Prior = 157,
		Return2 = 158,
		Separator = 159,
		Out = 160,
		Oper = 161,
		ClearAgain = 162,
		CrSel = 163,
		ExSel = 164,

		KP_00 = 176,
		KP_000 = 177,
		ThousandsSeparator = 178,
		DecimalSeparator = 179,
		CurrencyUnit = 180,
		CurrencySubUnit = 181,
		KP_LeftParen = 182,
		KP_RightParen = 183,
		KP_LeftBrace = 184,
		KP_RightBrace = 185,
		KP_Tab = 186,
		KP_Backspace = 187,
		KP_A = 188,
		KP_B = 189,
		KP_C = 190,
		KP_D = 191,
		KP_E = 192,
		KP_F = 193,
		KP_Xor = 194,
		KP_Power = 195,
		KP_Percent = 196,
		KP_Less = 197,
		KP_Greater = 198,
		KP_Ampersand = 199,
		KP_DblAmpersand = 200,
		KP_VerticalBar = 201,
		KP_DblVerticalBar = 202,
		KP_Colon = 203,
		KP_Hash = 204,
		KP_Space = 205,
		KP_At = 206,
		KP_Exclam = 207,
		KP_MemStore = 208,
		KP_MemRecall = 209,
		KP_MemClear = 210,
		KP_MemAdd = 211,
		KP_MemSubtract = 212,
		KP_MemMultiply = 213,
		KP_MemDivide = 214,
		KP_PlusMinus = 215,
		KP_Clear = 216,
		KP_ClearEntry = 217,
		KP_Binary = 218,
		KP_Octal = 219,
		KP_Decimal = 220,
		KP_Hexadecimal = 221,

		LCtrl = 224,
		LShift = 225,
		LAlt = 226, /**< alt, option */
		LGui = 227, /**< windows, command (apple), meta */
		RCtrl = 228,
		RShift = 229,
		RAlt = 230, /**< alt gr, option */
		RGui = 231, /**< windows, command (apple), meta */

		Mode = 257,    /**< i'm not sure if this is really not covered
					*   by any of the above, but since there's a
					*   special kmoD_Mode for it i'm adding it here
					*/

		AudioNext = 258,
		AudioPrev = 259,
		AudioStop = 260,
		AudioPlay = 261,
		AudioMute = 262,
		MediaSelect = 263,
		WWW = 264,
		Mail = 265,
		Calculator = 266,
		Computer = 267,
		AC_Search = 268,
		AC_Home = 269,
		AC_Back = 270,
		AC_Forward = 271,
		AC_Stop = 272,
		AC_Refresh = 273,
		AC_Bookmarks = 274,

		BrightnessDown = 275,
		BrightnessUp = 276,
		DisplaySwitch = 277, /**< display mirroring/dual display Switch, video mode switch */
		KBDillumToggle = 278,
		KBDillumDown = 279,
		KBDillumUp = 280,
		Eject = 281,
		Sleep = 282,

		App1 = 283,
		App2 = 284
	};
}

#endif
