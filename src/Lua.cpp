#include "Lua.h"
#include "lua.hpp"
#include "logtrace.h"

namespace Fusion
{
	Lua::Lua()
		: L(luaL_newstate())
	{
		luaL_openlibs(L);
	}

	Lua::~Lua()
	{
		lua_close(L);
	}

	bool Lua::execute(const std::string & file)
	{
		luaL_dofile(L, file.c_str());
		return false;
	}

	LuaAccessor Lua::operator[](const std::string & key)
	{
		return LuaAccessor(key, *this);
	}

	void Lua::field(int index, const std::string & key)
	{
		lua_getfield(L, index, key.c_str());
	}

	void Lua::global(const std::string & key)
	{
		lua_getglobal(L, key.c_str());
	}

	int Lua::top()
	{
		return lua_gettop(L);
	}

	void Lua::pop(long n)
	{
		return lua_pop(L, n);
	}

	void Lua::push(int value)
	{
		lua_pushinteger(L, value);
	}

	void Lua::push(double value)
	{
		lua_pushnumber(L, value);
	}

	void Lua::push(unsigned value)
	{
		lua_pushunsigned(L, value);
	}

	void Lua::push(bool value)
	{
		lua_pushboolean(L, value);
	}

	void Lua::push(const char  * value)
	{
		lua_pushstring(L, value);
	}

	void Lua::assign(int index, const std::string & key)
	{
		lua_setfield(L, index, key.c_str());
	}



	template<> int Lua::as<int>(int index)
	{
		return static_cast<int>(lua_tointeger(L, index));
	}

	template<> bool Lua::is<int>(int stackPosition)
	{
		return lua_isnumber(L, stackPosition) == 1;
	}

	template<> bool Lua::is<Lua::Table>(int stackPosition)
	{
		return lua_istable(L, stackPosition) == 1;
	}
}
