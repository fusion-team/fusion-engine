#include "EntityIndex.h"
#include "FusionConfig.h"
#include <unordered_map>

namespace Fusion
{

	EntityIndex::EntityIndex()
		: entityId2Index(PREALOC_ENTITY2IDX_SIZE, UINT32_MAX)
		, entityIndex2Id(PREALOC_ENTITY2IDX_SIZE, UINT32_MAX)
		, freeStore(1, 0)
		, entityName2Id()
	{
	}

	Entity::Id EntityIndex::nextId()
	{
		Entity::Id id = freeStore.back();
		freeStore.pop_back();
		if (freeStore.empty()) {
			Entity::Id next = id + 1;
			freeStore.push_back(next);
			while (next >= entityId2Index.size() || next >= entityIndex2Id.size())
			{
				entityId2Index.resize(entityId2Index.size() * 2);
				entityIndex2Id.resize(entityIndex2Id.size() * 2);
			}
		}
		return id;
	}

	EntityIndex::~EntityIndex()
	{

	}

	Entity::Index EntityIndex::id2Index(Entity::Id id)
	{
		if (id >= entityId2Index.size())
			return UINT32_MAX;
		return entityId2Index[id];
	}

	Entity::Id EntityIndex::index2Id(Entity::Index index)
	{
		if (index >= entityIndex2Id.size())
			return UINT32_MAX;
		return entityIndex2Id[index];
	}

	const std::vector<Entity::Id> & EntityIndex::withName(const std::string & name)
	{
		return entityName2Id[name];
	}

	Entity::Id EntityIndex::create(const std::string & name, Entity::Index index)
	{
		Entity::Id id = nextId();
		entityName2Id[name].push_back(id);
		entityId2Index[id] = index;
		entityIndex2Id[index] = id;
		return id;
	}

	void EntityIndex::destroy(Entity::Id id)
	{
		if (!valid(id)) return;
		Entity::Index index = id2Index(id);

		entityId2Index[id] = UINT32_MAX;
		entityIndex2Id[index] = UINT32_MAX;

		freeStore.push_back(id);
	}

	bool EntityIndex::valid(Entity::Id id)
	{
		size_t index = id2Index(id);
		return !(index == UINT32_MAX);
	}
}
