#ifndef FUSION_LUAENVIRONMENT_H
#define FUSION_LUAENVIRONMENT_H

#include "logtrace.h"
#include <string>

struct lua_State;

namespace Fusion
{
	class LuaEnvironment
	{
		public:
			class Accessor {
				public:
					Accessor(LuaEnvironment & env, const std::string & key, int16_t stack, int16_t parent);
					Accessor operator[](const std::string & field);
					//Accessor operator[](int index);
					template <typename... Ts>
					Accessor operator()(Ts &&... vs);

					template <typename T>
					T as(const T & def = T())
					{
						return def;
					}

					template <typename T>
					bool is()
					{
						return false;
					}

					template <typename T>
					Accessor & operator=(const T & value) {
						int16_t table = parent;
						if (parent == 0) table = pushGlobalTable();
						env.push(key);
						env.push(value);
						modinfo("lua-env");
						trace("VALORE EH", key, value);
						assign(table);
						return * this;
					}

				private:
					LuaEnvironment & env;
					int16_t stack;
					int16_t parent;
					std::string key;
					int16_t pushGlobalTable();
					void assign(int16_t table);
			};

		public:
			LuaEnvironment();
            void execute(const std::string & file);

			Accessor operator[](const std::string & global);

            template<typename T>
            void registerSubsystem(T& subsystem)
            {
                subsystem.luaRegistry(*this);
            }

		public:
            void push(const int& i);
            void push(const double& v);
			void push(const std::string & v);
            void push(const char * v) { push(std::string(v)); }
            void push(const bool& v);

			template<typename T, typename Q, typename... Ts> void push(T && t, Q && q, Ts &&... vs) {
				this->push(t);
				return push(q, vs...);
			};

		private:
			lua_State * L;
			friend class Accessor;
	};

    template<>
    bool LuaEnvironment::Accessor::is<int>();

    template<>
    bool LuaEnvironment::Accessor::is<double>();

    template<>
    bool LuaEnvironment::Accessor::is<std::string>();

    template<>
    int LuaEnvironment::Accessor::as<int>(const int & def);

    template<>
    double LuaEnvironment::Accessor::as<double>(const double & def);

    template<>
    std::string LuaEnvironment::Accessor::as<std::string>(const std::string & def);
}





#endif //STICKMANS_LUAENVIRONMENT_H
