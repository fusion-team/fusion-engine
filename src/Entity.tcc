#include "Entity.h"
#include "Scene.h"
#include "logtrace.h"
#include <cassert>
#include <utility>

namespace Fusion
{

	template <typename T, typename... Vs>
	void Entity::attach(Vs&&... vs)
	{
		if (attached<T>()) {
			modwarn("entity");
			trace("FIXME: Attempt to attach twice the same component to entity!");
		}
		scene.attach<T, Vs...>(index(), std::forward<Vs>(vs)...);
	}

	template <typename... Ts>
	bool Entity::attached()
	{
		Component::Mask testMask = Component::maskOf<Ts...>();
		return scene.attached(index(), testMask);
	}

	template <typename T>
	T & Entity::component()
	{
		assert(attached<T>());
		return scene.component<T>(index());
	}
}
