#ifndef FUSTION_MOTION_PROCESS_H
#define FUSTION_MOTION_PROCESS_H

#include "Processor.h"
#include "Renderer.h"

namespace Fusion
{
	class MotionProcessor : public Processor
	{
		public:
			MotionProcessor();
			void run(Scene & scene) override;
			~MotionProcessor();
	};
}

#endif // FUSTION_MOTION_PROCESS_H
