#include "Fusion.h"
#include "MotionProcessor.h"
#include "Entity.h"
#include "Scene.h"
#include "Component/TransformComponent.h"
#include "Component/MotionComponent.h"
#include "logtrace.h"
#include "Renderer.h"
#include "FusionMath.h"
#include "Loader/ImageLoader.h"
#include <cstdlib>

namespace Fusion
{
	MotionProcessor::MotionProcessor()
	{

	}


	void MotionProcessor::run(Fusion::Scene & scene)
	{
		for(Entity e : scene.select<MotionComponent, TransformComponent>())
		{
			MotionComponent & motion = e.component<MotionComponent>();
			TransformComponent & transform = e.component<TransformComponent>();

			Math::vec3 newVelocity = motion.velocity + motion.acceleration * 0.017f;
			transform.translate((motion.velocity + newVelocity) * 0.5f * 0.017f);
			motion.velocity = newVelocity;
		}
	}

	MotionProcessor::~MotionProcessor()
	{

	}
}

