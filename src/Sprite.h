#ifndef FUSION_SPRITE_H
#define FUSION_SPRITE_H

#include "FusionMath.h"
#include "Renderer.h"
#include "Camera.h"

namespace Fusion
{
	class Sprite
	{
		public:
			Sprite();
			Sprite(Sprite && other);
			Sprite(Renderer::Texture texture, Math::vec2 frameDimensions, Math::vec2 textureDimensions, Math::vec4 spriteRect);
			void render(Camera & camera, Math::vec2 frame = Math::vec2{0,0}, const Math::mat4 & objectTransform = Math::mat4{});

			void operator=(Sprite && other);

			Renderer::Texture & texture() { return t; }

		private:
			Renderer::Texture t;
			Renderer & renderer;
			float nFrames;
			float nStrips;
			Math::vec4 spriteRect;
			Math::vec2 frameDimensions;

		private:
			static void init();
			static Renderer::Mesh mesh;
			static Renderer::ShaderProgram shader;
	};
}

#endif //FUSION_SPRITE_H
