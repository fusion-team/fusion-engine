#include "Entity.h"
#include "FusionConfig.h"
#include "Scene.h"

namespace Fusion
{
	Entity::Id Entity::id()
	{
		return scene.idOf(entityIndex);
	}

	bool Entity::valid()
	{
		return entityIndex < UINT32_MAX;
	}

	Entity::Entity(Scene & scene, Index index)
		: scene(scene)
		, entityIndex(index)
	{

	}
}
