#include <algorithm>

#include "Scene.h"
#include "FusionConfig.h"
#include "Engine.h"
#include "Input.h"

#include "logtrace.h"

namespace Fusion
{
	Scene::Scene(Fusion::Engine & engine, Fusion::Input & input, const std::string & sceneName)
		: engine(engine)
		, input(input)
		, entityIndex()
		, attachedMasks(PREALOC_ENTITY2IDX_SIZE, 0)
		, sceneName(sceneName)

	{
		// TODO: run engine.lua().run(sceneName);
		(void) sceneName;
	}

	Entity Scene::entity(Entity::Id id)
	{
		return Entity{*this, entityIndex.id2Index(id)};
	}

	Entity::Id Scene::idOf(Entity::Index index)
	{
		return entityIndex.index2Id(index);
	}

	Entity Scene::entity(const std::string & name)
	{
		uint32_t nextIndex = componentPool.lease();
		entityIndex.create(name, nextIndex);

		//TODO: Run lua script <name>.lua
		return Entity(*this, nextIndex);
	}

	size_t Scene::entityCount()
	{
		return componentPool.count();
	}

	void Scene::destroy(Entity::Id id)
	{
		uint32_t oldIndex = entityIndex.id2Index(id); // don't trust index...
		componentPool.stale(oldIndex);
		entityIndex.destroy(id);
	}

	std::vector<Entity> Scene::entities(const std::string & entityName)
	{
		std::vector<Entity> entityVector;
		for (Entity::Id id : entityIndex.withName(entityName))
		{
			entityVector.emplace_back(*this, entityIndex.id2Index(id));
		}

		return entityVector;
	}

	bool Scene::attached(Entity::Index index, const Component::Mask & testMask)
	{
		return (testMask != 0) && ((attachedMasks[index] & testMask) == testMask);
	}

	void Scene::addProcessor(const std::string & processorName)
	{
		if (processorByName.find(processorName) != processorByName.end())
		{
			modwarn("scene");
			trace("Attempt to add process", processorName, "twice to", sceneName);
			return;
		}

		Processor & p = engine.processor(processorName);
		processors.push_back(&p);
		processorByName[processorName] = &p;
		if (p.metadata.keyboardListener) processorByInput.keyboard.push_back(&p);
		if (p.metadata.pointerListener) processorByInput.mouse.push_back(&p);
		if (p.metadata.wheelListener) processorByInput.wheel.push_back(&p);
	}

	void Scene::removeProcessor(const std::string & processorName)
	{
		Processor *& p = processorByName[processorName];
		if (p == nullptr)
		{
			modwarn("scene");
			trace("Attempt to remove non-added process", processorName, "from", sceneName);
			return;
		}

		p = nullptr;
		processors.erase(std::remove(processors.begin(), processors.end(), p), processors.end());
	}

	void Scene::registerComponent(Component::Id id, size_t size)
	{
		componentPool.zone(id, size);
	}

	void Scene::registerComponents(const std::vector<std::pair<Component::Id, size_t>> & idsAndSizes)
	{
		componentPool.zone(idsAndSizes);
	}

	void Scene::update()
	{
		input.update(processorByInput.keyboard, processorByInput.mouse, processorByInput.wheel);
		for (Processor *& p : processors)
		{
			p->run(*this);
		}
	}

	Scene::EntitySelector Scene::select(Component::Mask mask)
	{
		if (mask == 0) return EntitySelector(*this, mask, 0, 0);
		// TODO: implement range selector.
		return EntitySelector(*this, mask);
	}


	Scene::EntitySelector::EntitySelector(Scene & scene, Component::Mask mask, uint32_t first, uint32_t count)
		: scene(scene)
		, mask(mask)
		, index(first)
		, count(count)
	{

	}

	Scene::EntitySelector::EntitySelector(Scene & scene, Component::Mask mask)
		: scene(scene)
		, mask(mask)
		, index(0)
		, count(scene.entityCount())
	{ }

	Scene::EntitySelector::Iterator Scene::EntitySelector::begin()
	{
		return Iterator(scene, index, count, mask);
	}

	Scene::EntitySelector::Iterator Scene::EntitySelector::end()
	{
		return Iterator(scene, count, count, mask);
	}

	bool Scene::EntitySelector::Iterator::operator!=(const Iterator & other) const
	{
		return (index != other.index || mask != other.mask);
	}

	Entity Scene::EntitySelector::Iterator::operator*() const
	{
		return Entity{scene, index};
	}

	// Iterates one by one looking for an entity that matches
	const Scene::EntitySelector::Iterator & Scene::EntitySelector::Iterator::operator++()
	{
		// TODO: check caches
		while (++index < count)
		{
			if (scene.attached(index, mask)) break;
		}
		return *this;
	}

	Scene::EntitySelector::Iterator::Iterator(Scene & scene, uint32_t index, uint32_t count, Component::Mask mask)
		: scene(scene)
		, index(index)
		, count(count)
		, mask(mask)
	{ }
}
