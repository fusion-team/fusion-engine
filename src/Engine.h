#ifndef FUSION_ENGINE_H
#define FUSION_ENGINE_H

#include <string>
#include <unordered_map>
#include <vector>

#include "Component.h"
#include "Processor.h"

namespace Fusion
{
	class Scene;
	class Renderer;
	class Input;

	/**
	* @brief	The Engine class is the starting point for every game.
	* @details	The Engine keeps track of component registrations, scripting environment and
	* 			Lua runtime.
	*/
	class Engine
	{
		public:
			static Engine & instance();
			int run();

			Scene & scene(const std::string & sceneName);

			template <typename T>
			void registerComponent();

			template <typename T, typename Q, typename... Ts>
			void registerComponents();

			const std::vector<size_t> & knownComponents() { return componentSizes; }

			template <typename T>
			void registerProcessor(const std::string & processName);

			Processor & processor(const std::string & processName);

			~Engine();

		private:
			Engine();

		private:
			Scene * currentScene;
			std::unordered_map<std::string, Scene *> scenes;
			std::unordered_map<std::string, Processor *> registeredProcessors;
			std::vector<size_t> componentSizes;
			Component::Id nextId;
			Renderer & renderer;
			Input & input;
	};
}


#endif
