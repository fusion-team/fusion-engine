#include "glm/gtc/type_ptr.hpp"
#include "Renderer.h"
#include "glad/glad.h"
#include "SDL.h"

#include "FusionMath.h"

#include "logtrace.h"

#include <cmath>
#include <cstddef>
#include <algorithm>


#ifdef __ANDROID__
#define FUSION_SHADER_VERSION_STRING "#version 300 es\n"
#else
#define FUSION_SHADER_VERSION_STRING "#version 130\n"
#endif


namespace Fusion
{
	static const std::string shaderVersionString = FUSION_SHADER_VERSION_STRING;

	struct AttribLocation
	{
		enum
		{
			Vertex = 0,
			VertexNormal = 1,
			VertexColor = 2,
			VertexTextureMap = 3,
		};
	};

	Renderer & Renderer::instance()
	{
		static Renderer renderer;
		return renderer;
	}

    Renderer::Renderer()
	{
		SDL_GLprofile profile = SDL_GL_CONTEXT_PROFILE_CORE;

#ifdef __ANDROID__
		profile = SDL_GL_CONTEXT_PROFILE_ES;
#endif

		properties.windowDimensions = Math::vec2(800, 800);
		properties.fullscreen = false;
		properties.borderless = false;
		properties.title = "Fusion Engine Application";
		properties.vSyncOn = false;

		modinfo("renderer");
		if (!SDL_WasInit(SDL_INIT_VIDEO))
		{
			SDL_InitSubSystem(SDL_INIT_VIDEO);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, profile);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
		}

        int flags = SDL_WINDOW_OPENGL;
        if (properties.fullscreen) flags = flags | SDL_WINDOW_FULLSCREEN;
        else if (properties.borderless) flags = flags | SDL_WINDOW_BORDERLESS;

        window = SDL_CreateWindow(properties.title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                  properties.windowDimensions.x, properties.windowDimensions.y, flags);

		if (window == nullptr)
		{
			trace.e("Error creating OpenGL Window:", SDL_GetError());
		}
		int w,h;
		SDL_GetWindowSize(window, &w, &h);
		trace("Window size:", w, h);
		properties.windowDimensions.x = static_cast<float>(w);
		properties.windowDimensions.y = static_cast<float>(h);

		context = SDL_GL_CreateContext(window);
		trace("Context address:", context);
		if (context == nullptr)
		{
			trace.e("Error creating OpenGL context:", SDL_GetError());
		}

		if (!gladLoadGL())
		{
			trace.w("Unable to load OpenGL functions");
		}

		if (!gladLoadGLES2Loader(SDL_GL_GetProcAddress)) {
			trace.w("Unable to load OpenGLES functions");
		}

		glEnable (GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_TEXTURE_2D);

        SDL_GL_SetSwapInterval(properties.vSyncOn);

        GLint maxTextureUnits = 10;
        glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &maxTextureUnits);
        trace("There are", maxTextureUnits, "texture units available.");
		textureUnits.resize(maxTextureUnits);
		trace("GLSL version:", glGetString(GL_SHADING_LANGUAGE_VERSION));
	}

	Renderer::~Renderer()
	{
		if (SDL_WasInit(SDL_INIT_VIDEO))
		{
			SDL_QuitSubSystem(SDL_INIT_VIDEO);
		}
        if (context) SDL_GL_DeleteContext(context);
        if (window) SDL_DestroyWindow(window);
	}

	void Renderer::clear() {
		glBindVertexArray(0);
		glUseProgram(0);
		static float c1 = 0, c2 = 0, c3 = 0;
		c1 += 0.01f; c2 += 0.02f; c3 += 0.03f;
		glClearColor(sinf(c1), sinf(c2), sinf(c3), 1);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void Renderer::render(Mesh mesh, ShaderProgram shader)
	{
		glUseProgram(shader.id());
		glBindVertexArray(mesh.id());
		glDrawElements(GL_TRIANGLES, indexSizes[mesh.id()], GL_UNSIGNED_INT, 0);
	}

	void Renderer::swap()
	{
		SDL_GL_SwapWindow(window);
	}

	Renderer::ShaderProgram Renderer::compileShaders(const char * vertexShaderSource, const char * fragmentShaderSource, const ShaderProgram::Metadata & metadata)
	{
		GLuint shaderId = 0;
		GLuint programId = 0;

		programId = glCreateProgram();

		std::string shaderSource = shaderVersionString + vertexShaderSource;
		shaderId = compileShader(GL_VERTEX_SHADER, shaderSource.c_str());
		glAttachShader(programId, shaderId);

		shaderSource = shaderVersionString + fragmentShaderSource;
		shaderId = compileShader(GL_FRAGMENT_SHADER, shaderSource.c_str());
		glAttachShader(programId, shaderId);

		glBindAttribLocation(programId, AttribLocation::Vertex, metadata.vertexPoint.c_str());
		glBindAttribLocation(programId, AttribLocation::VertexNormal, metadata.vertexNormal.c_str());
		glBindAttribLocation(programId, AttribLocation::VertexColor, metadata.vertexColor.c_str());
		glBindAttribLocation(programId, AttribLocation::VertexTextureMap, metadata.vertexTextureMap.c_str());

		glLinkProgram(programId);

		GLint isLinked = 0;
		glGetProgramiv(programId, GL_LINK_STATUS, &isLinked);
		if (isLinked != GL_TRUE)
		{
			moderr("renderer");
			char programErrorLog[512];
			glGetProgramInfoLog(programId, 512, NULL, programErrorLog);
			glDeleteProgram(programId);
			programId = 0;
			trace.e("Linking shaders in program failed:", programErrorLog);
			return 0;
		}

		return ShaderProgram{programId, metadata};
	}

	Renderer::Mesh Renderer::uploadMesh(const std::vector<Vertex> & vertices, const std::vector<unsigned> & indexes)
	{
		GLuint vao = 0;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		// configure vertex buffer object
		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);


		// position data layout
		glEnableVertexAttribArray(AttribLocation::Vertex);
		glVertexAttribPointer(AttribLocation::Vertex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, position));

		// normals data layout
		glEnableVertexAttribArray(AttribLocation::VertexNormal);
		glVertexAttribPointer(AttribLocation::VertexNormal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, normal));

		// color data layout
		glEnableVertexAttribArray(AttribLocation::VertexColor);
		glVertexAttribPointer(AttribLocation::VertexColor, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, color));

		// uvmap data layout
		glEnableVertexAttribArray(AttribLocation::VertexTextureMap);
		glVertexAttribPointer(AttribLocation::VertexTextureMap, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, uvmap));


		// configure element buffer object
		GLuint ebo;
		glGenBuffers(1, &ebo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexes.size()*sizeof(unsigned), indexes.data(), GL_STATIC_DRAW);

		// everything is bound in the vao. restore state.
		glBindVertexArray(0);
		glDisableVertexAttribArray(AttribLocation::Vertex);
		glDisableVertexAttribArray(AttribLocation::VertexNormal);
		glDisableVertexAttribArray(AttribLocation::VertexColor);
		glDisableVertexAttribArray(AttribLocation::VertexTextureMap);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		// store index count of index array
		if (indexSizes.size() < vao)
			indexSizes.resize(vao +1, 0);

		indexSizes[vao] = indexes.size();

		return Mesh{vao};
	}

	unsigned Renderer::compileShader(unsigned shaderKind, const char * shaderSource)
	{
		if (shaderKind != GL_VERTEX_SHADER && shaderKind != GL_FRAGMENT_SHADER)
			return 0;

		const char * shaderKindString = (shaderKind == GL_VERTEX_SHADER) ? "Vertex" : "Fragment";
		GLuint shaderId = 0;

		// compile vertex shader
		shaderId = glCreateShader(shaderKind);
		glShaderSource(shaderId, 1, &shaderSource, NULL);
		glCompileShader(shaderId);

		GLint status = GL_FALSE;
		glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);
		//if (status != GL_TRUE)
		{
			moderr("renderer");
			char shaderErrorLog[512];
			glGetShaderInfoLog(shaderId, 512, NULL, shaderErrorLog);
			if (status != GL_TRUE)
			{
				glDeleteShader(shaderId);
				shaderId = 0;
			};
			trace.i(shaderKindString, "shader compile log below");
			trace.i(shaderErrorLog);
		}
		return shaderId;
	}

	Renderer::Texture Renderer::uploadTexture(void * pixels, unsigned width, unsigned height) {
		GLuint textureId = 0;
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);

		return textureId;
	}

	int32_t Renderer::activate(Texture t)
	{
		int32_t slot = 0;

		// see if texture is already active somewhere
		std::vector<int32_t>::iterator i;
		i = std::find(textureUnits.begin(), textureUnits.end(), t.id());
		if (i != textureUnits.end())
			return std::distance(textureUnits.begin(), i);

		// find a free slot
		i = std::find(textureUnits.begin(), textureUnits.end(), 0);
		if (i != textureUnits.end())
			slot = std::distance(textureUnits.begin(), i);
		else
			return 0;

		// set the texture id in that unit
		*i = t.id();

		// actually bind the texture
		glActiveTexture(GL_TEXTURE0 + slot);
		glBindTexture(GL_TEXTURE_2D, t.id());

		return slot;
	}

	void Renderer::deactivate(Texture t)
	{
		std::vector<int32_t>::iterator i;
		i = std::find(textureUnits.begin(), textureUnits.end(), t.id());
		if (i != textureUnits.end())
		{
			*i = 0;
			// it is *softly* deactivated, because texture unit binding can be
			// too much expensive just for cleanup
		}
	}

    void Renderer::setWindowTitle(const std::string& title)
    {
        SDL_SetWindowTitle(window, title.c_str());
        properties.title = title;
    }

    void Renderer::setWindowIcon(const std::string& path)
    {
        // TODO: Load and set icon
        properties.icon = path;
    }

    void Renderer::setWindowProperties(int x, int y, bool fullscreen, bool borderless)
    {
        properties.windowDimensions.x = x;
        properties.windowDimensions.y = y;
        properties.fullscreen = fullscreen;
        properties.borderless = borderless;

        if (context) SDL_GL_DeleteContext(context);
        if (window) SDL_DestroyWindow(window);

        int flags = SDL_WINDOW_OPENGL;
        if (fullscreen) flags = flags | SDL_WINDOW_FULLSCREEN;
        else if (borderless) flags = flags | SDL_WINDOW_BORDERLESS;

        window = SDL_CreateWindow(properties.title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                  properties.windowDimensions.x, properties.windowDimensions.y, flags);
        context = SDL_GL_CreateContext(window);

    }

    void Renderer::setVSync(bool vSync)
    {
        if (SDL_GL_SetSwapInterval(vSync)) {
            moderr("vSync");
            trace("Setting vsync failed: ", SDL_GetError());
            return;
        }
        properties.vSyncOn = vSync;
    }

}
