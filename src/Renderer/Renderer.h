#ifndef FUSION_RENDERER_H
#define FUSION_RENDERER_H

struct SDL_Window;
#include <vector>
#include <array>
#include <string>
#include "FusionMath.h"

namespace Fusion
{
	class Renderer {
		public:
			class ShaderProgram
			{
				public:
					struct Resource {
						uint32_t programId;
						int32_t resourceId;

						template <typename T>
						Resource & operator=(const T & value);

						template <typename T>
						operator T();
					};

					struct Metadata
					{
						public:
							Metadata() { }
							std::string vertexPoint = "Vertex";
							std::string vertexColor = "VertexColor";
							std::string vertexTextureMap = "VertexTextureMap";
							std::string vertexNormal = "VertexNormal";
							std::string modelMatrix = "ModelMatrix";
							std::string viewMatrix = "ViewMatrix";
							std::string projectionMatrix = "ProjectionMatrix";
					};

					struct UniformAccessor {
						uint32_t programId;
						Resource operator[](const std::string & name);
						Resource operator[](int32_t id);
					} uniform;

					Resource model;
					Resource view;
					Resource projection;

				public:
					ShaderProgram(uint32_t programId = 0, const Metadata & metadata = Metadata{});
					uint32_t id() { return programId; }

				private:
					uint32_t programId;
			};

			class Mesh
			{
				public:
					Mesh(uint32_t vertexArrayObject = 0) : vertexArrayObject(vertexArrayObject) { }
					uint32_t id() { return vertexArrayObject; }

				private:
					uint32_t vertexArrayObject;
			};

			/**
			 * @brief The Texture class references and manages an texture on GPU
			 * @details A Texture is invalid (i.e, points to nothing) if
			 *   id() returns 0.
			 * @sa Renderer::uploadTexture
			 */
			class Texture
			{
				public:
					/**
					 * @brief Texture constructs a new reference to a GPU texture
					 * @param textureId the GPU texture id to manage
					 * @sa Renderer::uploadTexture
					 */
					Texture(uint32_t textureId = 0) : textureId(textureId) { }

					/**
					 * @brief id Returns the GPU id for this texture.
					 * @return The id of the texture.
					 *         A valid texture has id() > 0.
					 */
					uint32_t id() { return textureId; }

					/**
					 * @brief activate Activates this texture on a texture unit
					 * @return The id of the texture unit or 0 if unsuccessful.
					 * @details Activating a texture multiple times has no effect (i.e, it will
					 * reuse the same texture unit).
					 * @code{c++}
					 * Renderer::Texture t;
					 * Renderer::ShaderProgram p;
					 * p.uniform["tex0"] = t.activate();
					 * @endcode
					 */
					int32_t activate();
					void deactivate();

				private:
					uint32_t textureId;
			};

			class Vertex
			{
				public:
					Math::vec3 position;
					Math::vec3 normal;
					Math::vec4 color;
					Math::vec2 uvmap;
			};

		public:
			static Renderer & instance();
			~Renderer();
			void swap();
			void clear();
			void render(Mesh mesh, ShaderProgram shader);
			ShaderProgram compileShaders(const char * vertex, const char * fragment, const ShaderProgram::Metadata & metadata = ShaderProgram::Metadata{});
			Mesh uploadMesh(const std::vector<Vertex> & vertices, const std::vector<unsigned> & indexes);
			Texture uploadTexture(void * pixels, unsigned width, unsigned height);

			int32_t activate(Texture t);
			void deactivate(Texture t);

			Math::mat4 view;
			Math::mat4 projection;

            int windowWidth() { return properties.windowDimensions.x; }
            int windowHeight() { return properties.windowDimensions.y; }
            float aspectRatio() { return ((float)windowWidth())/((float)windowHeight()); }
            Math::vec2 windowDimensions() { return properties.windowDimensions; }
            bool isFullscreen() { return properties.fullscreen; }
            bool isVSyncOn() { return properties.vSyncOn; }
            bool isBorderless() { return properties.borderless; }

            void setWindowTitle(const std::string& title);
            void setWindowIcon(const std::string& path);
            void setWindowProperties(int x, int y, bool fullscreen, bool borderless);
            void setVSync(bool vSync);

		private:
			SDL_Window * window;
			void * context;
			std::vector<unsigned> indexSizes;
			std::vector<int32_t> textureUnits;
            std::vector<std::array<int32_t, 3>> shaderResources;

            struct WindowProperties
            {
                Math::vec2 windowDimensions;
                bool fullscreen;
                bool borderless;
                bool vSyncOn;
                std::string title;
                std::string icon;
            };

            WindowProperties properties;

		private:
			Renderer();
			unsigned compileShader(unsigned shaderKind, const char * shaderSource);
	};
}

#endif // FUSION_RENDERER_H
