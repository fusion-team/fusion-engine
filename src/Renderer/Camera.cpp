#include "Camera.h"

using Fusion::Camera;
using Fusion::Renderer;

Camera::Camera()
    : renderer(Renderer::instance())
{
}

Camera::Camera(const Camera& other)
    : renderer(Renderer::instance()),
      viewMatrix(other.viewMatrix),
      projectionMatrix(other.projectionMatrix)
{
}

Camera::Camera(Camera&& other)
    : renderer(Renderer::instance())
{
    viewMatrix = std::move(other.viewMatrix);
    projectionMatrix = std::move(other.projectionMatrix);
}

void Camera::operator=(const Camera& other)
{
    viewMatrix = other.viewMatrix;
    projectionMatrix = other.projectionMatrix;
}

void Camera::render(const Fusion::Math::mat4 & transformMatrix, Renderer::Mesh mesh, Renderer::ShaderProgram program)
{
    program.model = transformMatrix;
    program.view = viewMatrix;
    program.projection = projectionMatrix;
    renderer.render(mesh, program);
}

void Camera::lookAt(const Fusion::Math::vec3 & eye, const Fusion::Math::vec3 & center, const Fusion::Math::vec3 & up)
{
    viewMatrix = Fusion::Math::lookAt(eye, center, up);
}

void Camera::setPerspective(float fovy, float aspectRatio, float zNear, float zFar)
{
    projectionMatrix = Math::perspective(fovy, aspectRatio, zNear, zFar);
}

void Camera::setOrtho(float left, float right, float bottom, float top, float zNear, float zFar)
{
    projectionMatrix = Math::ortho(left, right, bottom, top, zNear, zFar);
}

Camera Camera::makeOrthographic(float left, float right, float bottom, float top, float zNear, float zFar)
{
    Camera camera;
    camera.setOrtho(left, right, bottom, top, zNear, zFar);
    return camera;
}

Camera Camera::makePerspective(float fovy, float aspectRatio, float zNear, float zFar)
{
    Camera camera;
    camera.lookAt(Fusion::Math::vec3(0, 0, 1), Fusion::Math::vec3(0, 0, 0), Fusion::Math::vec3(0, 1, 0));
    camera.setPerspective(fovy, aspectRatio, zNear, zFar);
    return camera;
}
