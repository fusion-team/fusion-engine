#ifndef FUSION_CAMERA_H
#define FUSION_CAMERA_H

#include "FusionMath.h"
#include "Renderer.h"

namespace Fusion
{

class Camera
{
    public:
        Camera();
        Camera (const Camera& other);
        Camera (Camera&& other);

        void operator=(const Camera& other);

        void render(const Math::mat4 & transformMatrix,
                    Renderer::Mesh mesh,
                    Renderer::ShaderProgram program);

        void lookAt(const Math::vec3 & eye,
                    const Math::vec3 & center,
                    const Math::vec3 & up);

        void setPerspective(float fovy,
                            float aspectRatio,
                            float zNear,
                            float zFar);

        void setOrtho(float left,
                      float right,
                      float bottom,
                      float top,
                      float zNear,
                      float zFar);

        static Camera makeOrthographic(float left = -Renderer::instance().windowWidth()/2,
                                       float right = Renderer::instance().windowWidth()/2,
                                       float bottom = -Renderer::instance().windowHeight()/2,
                                       float top = Renderer::instance().windowHeight()/2,
                                       float zNear = - 10.1f,
                                       float zFar = 100.0f);
        static Camera makePerspective(float fovy = 45.0f,
                                      float aspectRatio = Renderer::instance().aspectRatio(),
                                      float zNear = 0.1f,
                                      float zFar = 100.0f);

    public:
        Renderer & renderer;

    private:
        Math::mat4 viewMatrix;
        Math::mat4 projectionMatrix;
};

}
#endif // FUSION_CAMERA_H
