#include "Renderer.h"

namespace Fusion
{
	int32_t Renderer::Texture::activate()
	{
		return Renderer::instance().activate(*this);
	}

	void Renderer::Texture::deactivate()
	{
		return Renderer::instance().deactivate(*this);
	}
}
