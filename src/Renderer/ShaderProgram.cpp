#include "Renderer.h"
#include "glad/glad.h"
#include "FusionMath.h"
#include "logtrace.h"
#include "glm/gtc/type_ptr.hpp"


using namespace Fusion::Math;

namespace Fusion {
	using ShaderResource = Renderer::ShaderProgram::Resource;
	using UniAccessor = Renderer::ShaderProgram::UniformAccessor;

	std::string uniformTypeToString(GLenum type)
	{
		switch (type)
		{
			case GL_FLOAT: return "float";
			case GL_FLOAT_VEC2: return "vec2";
			case GL_FLOAT_VEC3: return "vec3";
			case GL_FLOAT_VEC4: return "vec4";
			case GL_INT: return "int";
			case GL_INT_VEC2: return "ivec2";
			case GL_INT_VEC3: return "ivec3";
			case GL_INT_VEC4: return "ivec4";
			case GL_BOOL: return "bool";
			case GL_BOOL_VEC2: return "bvec2";
			case GL_BOOL_VEC3: return "bvec3";
			case GL_BOOL_VEC4: return "bvec4";
			case GL_FLOAT_MAT2: return "mat2";
			case GL_FLOAT_MAT3: return "mat3";
			case GL_FLOAT_MAT4: return "mat4";
			case GL_SAMPLER_2D: return "sampler2D";
			default: return "unknown";
		}
	}

	std::string getUniformNameAndType(uint32_t programId, uint32_t uniformId)
	{
		int bufSize;
		int size;
		GLenum type;
		glGetProgramiv(programId, GL_ACTIVE_UNIFORM_MAX_LENGTH, &bufSize);
		char name[bufSize];
		glGetActiveUniform(programId, uniformId, bufSize, NULL, &size, &type, name);
		return std::string{name} + " (which is a " + uniformTypeToString(type) + ")";
	}

#define CHECK_GL_ERROR \
	GLenum err = glGetError(); \
	if (err == GL_INVALID_OPERATION) \
	{ \
		if (resourceId > 0) \
		{ \
			moderr("shader-program"); \
			trace("Could not set variable", getUniformNameAndType(programId, resourceId)); \
			trace("Possible incompatible types?"); \
		} \
	}


	ShaderResource UniAccessor::operator[](const std::string & name)
	{
		if (programId == 0 || name.size() == 0)
			return ShaderResource{0, -1};

		int32_t resourceId = glGetUniformLocation(programId, name.c_str());
		if (resourceId < 0)
		{
			moderr("shader-program");
			trace("Unable to find shader uniform variable", name);
			trace("It might be removed by optimization. Are you using it?");
		}
		return ShaderResource{programId, resourceId};
	}

	ShaderResource UniAccessor::operator[](int32_t resourceId)
	{
		return ShaderResource{programId, resourceId};
	}

	template <>
	ShaderResource & ShaderResource::operator=<float>(const float & value)
	{
		glUseProgram(programId);
		glUniform1f(resourceId, value);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<vec2>(const vec2 & value)
	{
		glUseProgram(programId);
		glUniform2f(resourceId, value.x, value.y);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<vec3>(const vec3 & value)
	{
		glUseProgram(programId);
		glUniform3f(resourceId, value.x, value.y, value.z);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<vec4>(const vec4 & value)
	{
		glUseProgram(programId);
		glUniform4f(resourceId, value.x, value.y, value.z, value.w);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<int>(const int & value)
	{
		glUseProgram(programId);
		glUniform1i(resourceId, value);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource::operator int()
	{
		int value;
		glUseProgram(programId);
		glGetUniformiv(programId, resourceId, &value);
		CHECK_GL_ERROR
		return value;
	}

	template <>
	ShaderResource & ShaderResource::operator=<ivec2>(const ivec2 & value)
	{
		glUseProgram(programId);
		glUniform2i(resourceId, value.x, value.y);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<ivec3>(const ivec3 & value)
	{
		glUseProgram(programId);
		glUniform3i(resourceId, value.x, value.y, value.z);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<ivec4>(const ivec4 & value)
	{
		glUseProgram(programId);
		glUniform4i(resourceId, value.x, value.y, value.z, value.w);
		CHECK_GL_ERROR
		return *this;
	}

#ifndef __ANDROID__
	template <>
	ShaderResource & ShaderResource::operator=<unsigned>(const unsigned & value)
	{
		glUseProgram(programId);
		glUniform1ui(resourceId, value);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<uvec2>(const uvec2 & value)
	{
		glUseProgram(programId);
		glUniform2ui(resourceId, value.x, value.y);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<uvec3>(const uvec3 & value)
	{
		glUseProgram(programId);
		glUniform3ui(resourceId, value.x, value.y, value.z);
		CHECK_GL_ERROR
		return *this;
	}

	template <>
	ShaderResource & ShaderResource::operator=<uvec4>(const uvec4 & value)
	{
		glUseProgram(programId);
		glUniform4ui(resourceId, value.x, value.y, value.z, value.w);
		CHECK_GL_ERROR
		return *this;
	}
#endif
	template <>
	ShaderResource & ShaderResource::operator=<mat4>(const mat4 & value)
	{
		glUseProgram(programId);
		glUniformMatrix4fv(resourceId, 1, GL_FALSE, glm::value_ptr(value));
		CHECK_GL_ERROR
		return *this;
	}

	Renderer::ShaderProgram::ShaderProgram(uint32_t programId, const Renderer::ShaderProgram::Metadata & metadata)
		: uniform{programId}
		, model{programId, uniform[metadata.modelMatrix].resourceId}
		, view{programId, uniform[metadata.viewMatrix].resourceId}
		, projection{programId, uniform[metadata.projectionMatrix].resourceId}
		, programId{programId}
	{
		if (programId <= 0)
			return;
		model = Fusion::Math::mat4();
		view = Fusion::Math::mat4();
		projection = Fusion::Math::mat4();
	}
}
