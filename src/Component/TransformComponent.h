#ifndef TRANSFORMCOMPONENT_H
#define TRANSFORMCOMPONENT_H

#include "Component.h"
#include "FusionMath.h"

namespace Fusion
{
	struct TransformComponent : Fusion::Component
	{
		public:
			TransformComponent(const Math::vec3 & position = Math::vec3(0.0f, 0.0f, 0.0f), const Math::vec3 & scale = Math::vec3(1.0f, 1.0f, 1.0f),
					  float angle = 0.0f, const Math::vec3 & rotation = Math::vec3(0.0f, 0.0f, 1.0f));

			static Component::Id id;
			void setPosition(const Math::vec3 & position);
			void setRotation(float angle, const Math::vec3 & rotation);
			void setScale(const Math::vec3 & scale);

			void translate(const Math::vec3 & position);
			void rotate(float angle, const Math::vec3 & rotation);
			void scale(const Math::vec3 & scale);

			const Math::mat4 & matrix();

		private:
			void calculateMatrix();

		private:
			Math::vec3 transformPosition;
			Math::vec3 transformRotation;
			Math::vec3 transformScale;
			Math::mat4 transformMatrix;
			float transformAngle;
	};
}

#endif // TRANSFORMCOMPONENT_H
