#ifndef FUSION_MOTION_COMPONENT_H
#define FUSION_MOTION_COMPONENT_H

#include "Component.h"
#include "FusionMath.h"

namespace Fusion
{
	class MotionComponent: public Fusion::Component
	{
		public:
			MotionComponent(Math::vec3 acceleration = Math::vec3(), Math::vec3 velocity = Math::vec3());

		public:
			static Component::Id id;
			Math::vec3 acceleration;
			Math::vec3 velocity;
	};
}
#endif // FUSION_MOTION_COMPONENT_H
