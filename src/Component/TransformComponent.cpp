#include "TransformComponent.h"

Fusion::Component::Id Fusion::TransformComponent::id;

namespace Fusion
{
	TransformComponent::TransformComponent(const Math::vec3 & position, const Math::vec3 & scale, float angle, const Math::vec3 & rotation)
		: transformPosition(position)
		, transformRotation(rotation)
		, transformScale(scale)
		, transformAngle(angle)
	{
		calculateMatrix();
	}

	void TransformComponent::translate(const Math::vec3 & position)
	{
		transformPosition = position;

		transformMatrix = Math::translate(transformMatrix, position);
	}

	void TransformComponent::rotate(float angle, const Math::vec3 & rotation)
	{
		transformRotation = rotation;
		transformAngle = angle;

		transformMatrix = Math::rotate(transformMatrix, angle, rotation);
	}

	void TransformComponent::scale(const Math::vec3 & scale)
	{
		transformScale = scale;

		transformMatrix = Math::scale(transformMatrix, scale);
	}

	void TransformComponent::setPosition(const Math::vec3 & position)
	{
		transformPosition = position;
		transformMatrix = Math::mat4(1.0f);

		calculateMatrix();
	}

	void TransformComponent::setRotation(float angle, const Math::vec3 & rotation)
	{
		transformRotation = rotation;
		transformAngle = angle;
		transformMatrix = Math::mat4(1.0f);

		calculateMatrix();
	}

	void TransformComponent::setScale(const Math::vec3 & scale)
	{
		transformScale = scale;
		transformMatrix = Math::mat4(1.0f);

		calculateMatrix();

	}

	void TransformComponent::calculateMatrix()
	{
		translate(transformPosition);
		scale(transformScale);
		rotate(transformAngle, transformRotation);
	}

	const Math::mat4 & TransformComponent::matrix()
	{
		return transformMatrix;
	}
}
