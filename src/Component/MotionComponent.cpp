#include "MotionComponent.h"

Fusion::Component::Id Fusion::MotionComponent::id;

namespace Fusion
{
	MotionComponent::MotionComponent(Math::vec3 acceleration, Math::vec3 velocity)
		: acceleration(acceleration)
		, velocity(velocity)
	{

	}
}
