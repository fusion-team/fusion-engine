#ifndef FUSION_ENTITYINDEX_H
#define FUSION_ENTITYINDEX_H

#include <vector>
#include <unordered_map>
#include "Entity.h"

/**
 * @brief The EntityIndex class manages the mapping between entity ids and their
 *        component index.
 */

namespace Fusion
{
	class EntityIndex
	{
		public:
			~EntityIndex();
			Entity::Index id2Index(Entity::Id id);
			Entity::Id index2Id(Entity::Index index);
			const std::vector<Entity::Id> & withName(const std::string & name);
			Entity::Id create(const std::string & name, Entity::Index index);
			void destroy(Entity::Id id);
			bool valid(Entity::Id id);

		private:
			EntityIndex();
			Entity::Id nextId();

		private:
			std::vector<Entity::Index> entityId2Index;
			std::vector<Entity::Id> entityIndex2Id;
			std::vector<Entity::Id> freeStore;
			std::unordered_map<std::string, std::vector<Entity::Id>> entityName2Id;
			friend class Scene;
			friend class Selector;
	};

	class Selector {
		public:
			Entity::Index start;
			Entity::Index end;
			EntityIndex & index;
	};
}
#endif // FUSION_ENTITYINDEX_H
