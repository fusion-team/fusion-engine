#ifndef FUSION_DATASTREAM_H
#define FUSION_DATASTREAM_H

#include <future>
#include <vector>

namespace Fusion
{
	class DataStream
	{
		public:
			static DataStream fromFile(const std::string & file, const char * modes = "rwb+");
			static DataStream fromMemory(void * mem, unsigned size);
			static DataStream fromMemory(const void * mem, unsigned size);

		public:
			std::vector<unsigned char> read(unsigned size);
			std::vector<unsigned char> readAll();
			int write(const std::vector<unsigned char> & data);
			int write(const void * data, unsigned size);
			int write(const std::string & data);
			int seek(int pos);
			bool eof() { return atEOF; }
			bool valid() { return impl != nullptr; }

			DataStream(DataStream && other);
			DataStream & operator=(const DataStream & other) = delete;
			DataStream & operator=(DataStream && other) = delete;
			DataStream(const DataStream & other) = delete;
			~DataStream();

		private:
			DataStream(void * impl);
			void * impl;
			bool atEOF;

		public:
			static std::string root;
	};
}

#endif
