#ifndef FUSION_TEXTSTREAM_H
#define FUSION_TEXTSTREAM_H

#include <string>
#include "DataStream.h"

namespace Fusion
{
    class DataStream;
    class TextStream
    {
        public:
            static TextStream fromFile(const std::string & file, const char * modes = "rw+");
            static TextStream fromMemory(void * mem, unsigned size);
            static TextStream fromMemory(const void * mem, unsigned size);
            std::string read(unsigned int size = 1);
            std::string readAll();
            char readChar();
            std::string readLine();
            std::string readWord();
            int write(const std::string & str);
            int seek(int pos);

        public:
            TextStream(TextStream && other);
            TextStream & operator=(const TextStream & other) = delete;
            TextStream & operator=(TextStream && other) = delete;
            TextStream(const TextStream & other) = delete;

            bool eof();
            bool valid();

        private:
            TextStream(const std::string & file, const char * modes = "rw+");
            TextStream(void * mem, unsigned size);
            TextStream(const void * mem, unsigned size);

        private:
            DataStream dataStream;

        public:
            static std::string root;
    };
}

#endif //FUSION_TEXTSTREAM_H
