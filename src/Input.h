#ifndef FUSION_INPUT_H
#define FUSION_INPUT_H

#include <cstdint>
#include <vector>
#include "InputTriggers.h"
#include "FusionMath.h"
#include <array>

namespace Fusion
{
	class Processor;
    class LuaEnvironment;
	class Input
	{
		public:
			enum PressState {
				Unpressed, /*!<< Button is not pressed and was not pressed before */
				Pressed,   /*!<< Button is just pressed and it was not pressed before */
				Pressing,  /*!<< Button is pressed and it was pressed before */
				Released   /*!<< Button is not pressed but it was pressed before */
			};

			class PointerButton {
				public:
					PressState state;
					float pressure;
			};

			class Pointer {
				public:
					float x, y;
					float dx, dy;
					std::array<PointerButton, 5> button;
					PointerButton & operator[](unsigned index)
					{
						return button[index];
					}
			};

			/**
			 * @brief instance Returns a reference to the Input system instance.
			 * @return A reference to Fusion::Input
			 */
			static Input & instance();

			/**
			 * @brief unpressed Check if a Keyboard KeyboardKey is not pressed.
			 * @param k Which key to test.
			 * @return Bool if KeyboardKey k is not pressed and was not pressed previously.
			 */
			inline bool unpressed(KeyboardKey k) { return state(k) == Unpressed; }

			/**
			 * @brief pressed Check if a Keyboard KeyboardKey is pressed.
			 * @param k Which key to test.
			 * @return Bool if KeyboardKey k is pressed and was not pressed previously.
			 */
			inline bool pressed(KeyboardKey k) { return state(k) == Pressed; }

			/**
			 * @brief released Check if a Keyboard KeyboardKey is released.
			 * @param k Which key to test.
			 * @return Bool if KeyboardKey k is not pressed but was pressed previously.
			 */
			inline bool released(KeyboardKey k) { return state(k) == Released; }

			/**
			 * @brief pressing Check if a Keyboard KeyboardKey is continuously being pressed.
			 * @param k Which key to test.
			 * @return Bool if KeyboardKey k is pressed and it was being pressed already.
			 */
			inline bool pressing(KeyboardKey k) { return state(k) == Pressing; }

			/**
			 * @brief quitRequested Check if quit was requested
			 * @return true if quit was requested, false otherwise.
			 */
			inline bool quitRequested() { return inputQuitRequested; }

			/**
			 * @brief mouseWheel retrieves horizontal or vertical mouse wheel movement in this frame.
			 * @param w Which wheel to check, horizontal or vertical.
			 * @return -1, 0 or 1 depending on which direction the wheel was rotated. -1 is left/down and +1 is right/up.
			 * @sa MouseWheel
			 */
			inline int32_t mouseWheel(MouseWheel w) { return (w == MouseWheel::Horizontal) ? wheelValues.x : wheelValues.y; }

			/**
			 * @brief mouseWheel
			 * @return a pair with both left and right wheel rotation values. The first value is for the horizontal wheel, the second for the vertical.
			 * @sa MouseWheel
			 */
			inline std::pair<int32_t, int32_t> mouseWheel() { return std::make_pair(wheelValues.x, wheelValues.y); }

			/**
			* @brief state Returns the state of a KeyboardKey
			 * @param k the key to test.
			 * @return A PressState enum representing the state of the key.
			 * @sa PressState
			 */
			inline PressState state(KeyboardKey k) { return keyboardState[static_cast<int>(k)]; }

			void update(std::vector<Processor *> keyboard, std::vector<Processor *> mouse, std::vector<Processor *> wheelListeners);

            void luaRegistry(Fusion::LuaEnvironment& env);

		private:
			std::vector<PressState> keyboardState; /* holds the state of all keys in each frame */
			Math::ivec2 wheelValues; /* wheel positions */
			std::vector<Pointer> pointers; /* pointers */
			bool inputQuitRequested;

		private:
			Input(); /* forbid public construction */
	};

}

#endif // INPUT_H
