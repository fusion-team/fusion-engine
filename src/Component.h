#ifndef FUSION_COMPONENT_H
#define FUSION_COMPONENT_H
#include <bitset>
#include <climits>
#include <type_traits>

#include "FusionConfig.h"

/* define underlying type of Component::Id */
#if MAX_COMPONENT_TYPES <= UINT8_MAX
#define COMPONENTID_TYPE uint8_t
#elif MAX_COMPONENT_TYPES <= UINT16_MAX
#define COMPONENTID_TYPE uint16_t
#elif MAX_COMPONENT_TYPES <= UINT32_MAX
#define COMPONENTID_TYPE uint32_t
#elif MAX_COMPONENT_TYPES <= UINT64_MAX
#define COMPONENTID_TYPE uint64_t
#else
#error MAX_COMPONENT_TYPES cannot be bigger than UINT64_MAX
#endif

#define W(x) #x

namespace Fusion
{

	template<class...> struct voider { using type = void; };
	template <class... Ts> using void_t = typename voider<Ts...>::type;

	template <typename T, class = void>
	struct HasIDMember : std::false_type { };

	template <typename T>
	struct HasIDMember<T, void_t<typename std::enable_if<std::is_same<COMPONENTID_TYPE, decltype(T::id)>::value, void>::type>> : std::true_type { };

	/**
	 * @brief		The Component struct is the base for all components.
	 * @details		All components should Inherit from this class AND have a Component::Id field in them.
	 */
	struct Component
	{
		public:
			typedef std::bitset<MAX_COMPONENT_TYPES> Mask; /// Defines the type of component masks
			typedef COMPONENTID_TYPE Id; /// Defines the type for component IDs.

			/**
			 * @brief maskOf Calculates and retrieves the mask of a single component.
			 * @return A ComponentMask with the bit corresponding to this component set.
			* @example
			* @code
			* maskOf<PositionComponent>
			*/
			template<typename T> static Component::Mask maskOf() {
				static_assert(HasIDMember<T>::value, "Class does not have 'id' member of type Component::Id");
				Mask mask;
				if (T::id > 0) mask[T::id -1] = 1;
				return mask;
			}

			/**
			* @brief maskOf Calculates and retrieves a mask for the component types.
			* @return A ComponentMask selection comprising every component.
			* @example
			* @code
			* maskOf<PositionComponent, VelocityComponent>()
			* @endcode
			*/
			template<typename T, typename Q, typename... Ts> static Component::Mask maskOf() {
				return maskOf<T>() | maskOf<Q, Ts...>();
			}

		protected:
			Component() : active(true) { }
			virtual ~Component() { active = false; }

		private:
			bool active;
			friend class ComponentPool;
	};
}
#endif // FUSION_COMPONENT_H

