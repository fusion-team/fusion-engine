#include "Sprite.h"
#include "Renderer.h"
#include <vector>

#define stringify(x) #x

namespace Fusion
{

	Sprite::Sprite()
		: renderer(Renderer::instance())
	{
		init();
	};

	Sprite::Sprite(Sprite && other)
		: t(std::move(other.t))
		, renderer(Renderer::instance())
		, nFrames(other.nFrames)
		, nStrips(other.nStrips)
		, spriteRect(std::move(other.spriteRect))
		, frameDimensions(std::move(other.frameDimensions))
	{
		init();
	}

	void Sprite::operator=(Sprite && other)
	{
		t  = std::move(other.t);
		nFrames = other.nFrames;
		nStrips = other.nStrips;
		spriteRect = std::move(other.spriteRect);
		frameDimensions = std::move(other.frameDimensions);
	}

	Sprite::Sprite(Renderer::Texture texture, Math::vec2 frameDimensions, Math::vec2 textureDimensions,
				   Math::vec4 spriteRect)
			: t(texture), renderer(Renderer::instance()),
			  nFrames(spriteRect.z / frameDimensions.x), nStrips(spriteRect.w / frameDimensions.y),
			  spriteRect(spriteRect.x / textureDimensions.x,
						 spriteRect.y / textureDimensions.y,
						 spriteRect.z / (nFrames * textureDimensions.x),
						 spriteRect.w / (nStrips * textureDimensions.y))
			, frameDimensions(frameDimensions)
	{
		init();
	}

	void Sprite::init()
	{
		if (mesh.id() == 0)
		{
			std::vector <Fusion::Renderer::Vertex> vertices = {
					{
							{-.5f, .5f,  0.0f}, // top-left
							{},
							{1.0, 0.0f, 1.0f, 1.0f},
							{0.0,  0.0f}
					},
					{
							{0.5f,  0.5f,  0.0f}, // top-right
							{},
							{1.0, 1.0f, 0.0f, 1.0f},
							{1.0,  0.0f}
					},
					{
							{.5f,  -.5f, 0.0f}, // bottom-right
							{},
							{1.0, 0.0f, 0.0f, 1.0f},
							{1.0f, 1.0f}
					},
					{
							{-.5f, -.5f, 0.0f}, //bottom-left
							{},
							{0.0, 0.0f, 1.0f, 1.0f},
							{0.0,  1.0f}
					}
			};

			std::vector<unsigned> idx = {0, 2, 3, 0, 1, 2};

			mesh = Renderer::instance().uploadMesh(vertices, idx);
		}

		if (shader.id() == 0)
		{
			const char * vs = stringify(
				in vec3 Vertex;
				in vec2 VertexTextureMap;
				in vec4 VertexColor;
				uniform vec4 SpriteRect;
				uniform vec2 FramePosition;
				uniform vec2 FrameDimensions;
				uniform mat4 ModelMatrix;
				uniform mat4 ViewMatrix;
				uniform mat4 ProjectionMatrix;
				out vec2 TextureMap;
				out vec4 Color;
				void main() {
					mat4 mvp = ProjectionMatrix * ViewMatrix * ModelMatrix;

					Color = VertexColor;
					TextureMap.x = SpriteRect.x + (SpriteRect.z * FramePosition.x) + (SpriteRect.z * VertexTextureMap.x);
					TextureMap.y = SpriteRect.y + (SpriteRect.w * FramePosition.y) + (SpriteRect.w * VertexTextureMap.y);
					gl_Position = mvp * vec4(Vertex * vec3(FrameDimensions, 1.0), 1.0);
				}
			);

			const char * fs = stringify(
				precision highp float;
				precision highp int;
				in vec2 TextureMap;
				in vec4 Color;
				uniform sampler2D tru;
				out vec4 OutputColor;
				void main() {
					OutputColor = texture(tru, TextureMap);
				}
			);
			shader = Renderer::instance().compileShaders(vs, fs);
		}
	}

	void Sprite::render(Camera & camera, Math::vec2 framePosition, const Math::mat4 & objectTransform)
	{
		shader.uniform["SpriteRect"] = spriteRect;
		shader.uniform["FramePosition"] = framePosition;
		shader.uniform["FrameDimensions"] = frameDimensions;
		shader.uniform["tru"] = 0;
		camera.render(objectTransform, mesh, shader);
	}
}



