#include "Engine.h"
#include "Scene.h"
#include "Renderer.h"
#include "Input.h"
#include "SDL2_framerate.h"
#include "logtrace.h"
#include <cassert>
#include <ctime>
#include <cstdlib>

namespace Fusion
{
	int Engine::run()
	{
		modinfo("engine");
		bool running = true;
		FPSmanager fpsManager;
		SDL_initFramerate(&fpsManager);
		while (running)
		{
			if (currentScene == nullptr)
				continue;

			renderer.clear();
			currentScene->update();
			renderer.swap();
			SDL_framerateDelay(&fpsManager);
		}
		return 0;
	}

	Engine::Engine()
		: currentScene(nullptr)
		, scenes()
		, registeredProcessors()
		, componentSizes(MAX_COMPONENT_TYPES)
		, nextId(0)
		, renderer(Renderer::instance())
		, input(Input::instance())
	{
		SDL_Init(SDL_INIT_EVERYTHING);
		// 0th component is never valid.
		componentSizes.push_back(0);
		std::srand(std::time(nullptr));
	}

	Engine & Engine::instance()
	{
		static Engine engine;
		return engine;
	}

	Engine::~Engine()
	{
		for (auto && s : scenes)
			delete s.second;

		for (auto && p : registeredProcessors)
			delete p.second;

		SDL_Quit();
	}

	Scene & Engine::scene(const std::string & sceneName)
	{
		auto i = scenes.find(sceneName);
		Scene * selectedScene = nullptr;
		// scene was not found, one needs to be instantiated.
		if (i == scenes.end())
		{
			Scene * newScene = new Scene(*this, input, sceneName);
			scenes[sceneName] = newScene;
			if (currentScene == nullptr)
				currentScene = newScene;
			selectedScene = newScene;
		}
		else
		{
			selectedScene = i->second;
		}
		return *selectedScene;
	}

	Processor & Engine::processor(const std::string & processorName)
	{
		Processor * requiredProcessor = registeredProcessors[processorName];
		assert(requiredProcessor != nullptr);
		return *requiredProcessor;
	}

}
