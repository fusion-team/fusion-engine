#ifndef FUSION_COMPONENTPOOL_H
#define FUSION_COMPONENTPOOL_H

#include <array>
#include <vector>
#include "Component.h"
#include "FusionConfig.h"

namespace Fusion
{

	class ComponentPool
	{
		public:
			void zone(Component::Id id, size_t size);
			void zone(const std::vector<std::pair<Component::Id, size_t>> & shared);
			size_t count();

			uint32_t lease();
			void stale(uint32_t index);

			struct ComponentStorage
			{
					std::vector<uint8_t *> * chunks = nullptr;
					size_t blockSize = 0;
					size_t offset = 0;
					uint8_t * cachedChunk;
					size_t cachedChunkIndex;
					uint8_t * operator[](uint32_t index);
			};

			ComponentStorage & operator[](Component::Id id);
		private:
			ComponentPool();
			~ComponentPool();
			std::array<ComponentStorage, MAX_COMPONENT_TYPES> componentStorage;
			std::vector<uint32_t> staleIndexes;
			uint32_t lastIndex;

			friend class Scene;
	};

}

#endif // FUSION_COMPONENTPOOL_H
