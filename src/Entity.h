#ifndef FUSION_ENTITY_H
#define FUSION_ENTITY_H

#include <string>

namespace Fusion
{
	class Engine;
	class Scene;

	class Entity
	{
		public:
			// these are defined for type safety
			struct Id
			{
					uint32_t id;
					Id(uint32_t id = 0) : id(id) { }
					operator uint32_t() { return id; }
			};

			struct Index
			{
					uint32_t index;
					Index(uint32_t index = 0) : index(index) { }
					operator uint32_t() { return index; }
			};

			Entity(Scene & scene, Entity::Index index);
			Entity::Id id();
			Entity::Index index() { return entityIndex; }
			bool valid();

			template <typename T, typename... Vs>
			void attach(Vs&&... vs);

			template <typename... Ts>
			bool attached();

			template <typename T>
			T & component();

		private:
			Scene & scene;
			Entity::Index entityIndex;
	};
}

#endif // FUSION_ENTITY_H
