#include "ImageLoader.h"
#include "DataStream.h"

#include "SDL.h"
#include "SDL_image.h"

#include "logtrace.h"

namespace Fusion
{

	SDL_Surface * createTargetSurface(unsigned width, unsigned height);
	static unsigned nextPowerOf2(unsigned n);

	ImageLoader::ImageLoader()
	{
	}

	Image ImageLoader::load(const std::string & file)
	{
		DataStream ds =  DataStream::fromFile(file);
		std::vector<unsigned char> data = ds.readAll();
		if (data.size() == 0)
			return Image{nullptr, 0, 0};

		SDL_RWops * rw = SDL_RWFromMem(data.data(), data.size());
		SDL_Surface * s = IMG_Load_RW(rw, 0);
		SDL_RWclose(rw);
		if (s == nullptr)
			return Image{nullptr, 0, 0};

		SDL_Surface * target = createTargetSurface(nextPowerOf2(s->w), nextPowerOf2(s->h));
		SDL_SetSurfaceBlendMode(s, SDL_BLENDMODE_NONE);
		SDL_BlitScaled(s, NULL, target, NULL);
		SDL_FreeSurface(s);

		bool mustLock = SDL_MUSTLOCK(target);
		if (mustLock) SDL_LockSurface(target);
		unsigned char * pixels = static_cast<unsigned char *>(target->pixels);
		unsigned w = static_cast<unsigned>(target->w);
		unsigned h = static_cast<unsigned>(target->h);
		Image img{pixels, w, h};
		if (mustLock) SDL_UnlockSurface(target);
		SDL_FreeSurface(target);
		return img;
	}

	SDL_Surface * createTargetSurface(unsigned width, unsigned height)
	{
		unsigned rmask, gmask, bmask, amask;
		SDL_Surface * s = NULL;
		#if SDL_BYTEORDER == SDL_BIG_ENDIAN
		rmask = 0xff000000;
		gmask = 0x00ff0000;
		bmask = 0x0000ff00;
		amask = 0x000000ff;
		#else
		rmask = 0x000000ff;
		gmask = 0x0000ff00;
		bmask = 0x00ff0000;
		amask = 0xff000000;
		#endif
		s = SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);
		if (s != NULL) {
			SDL_SetSurfaceBlendMode(s, SDL_BLENDMODE_NONE);
		}
		return s;
	}

	static unsigned nextPowerOf2(unsigned n)
	{
		n--;
		n |= n >> 1;
		n |= n >> 2;
		n |= n >> 4;
		n |= n >> 8;
		n |= n >> 16;
		n++;
		return n;
	}
}
