#ifndef FUSION_IMAGELOADER_H
#define FUSION_IMAGELOADER_H

#include <string>
#include "Resource/Image.h"

namespace Fusion
{
	class ImageLoader
	{
		public:
			ImageLoader();
			Image load(const std::string & file);
	};
}

#endif
