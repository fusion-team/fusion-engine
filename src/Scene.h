#ifndef FUSION_SCENE_H
#define FUSION_SCENE_H

#include <cinttypes>
#include <string>
#include <vector>
#include <unordered_map>

#include "Entity.h"
#include "EntityIndex.h"
#include "ComponentPool.h"
#include "Component.h"
#include "Processor.h"

namespace Fusion
{
	class Engine;
	class Input;

	class Scene
	{
		public:
			class EntitySelector {
				public:
					class Iterator
					{
						public:
							Iterator(Scene & scene, uint32_t index, uint32_t count, Component::Mask mask);
							bool operator!=(const Iterator & other) const;
							Entity operator*() const;
							const Iterator & operator++();
						private:
							Scene & scene;
							uint32_t index;
							uint32_t count;
							Component::Mask mask;
					};
				public:
					EntitySelector(Scene & scene, Component::Mask mask);
					EntitySelector(Scene & scene, Component::Mask mask, uint32_t first, uint32_t count);
					Iterator begin();
					Iterator end();

				private:
					Scene & scene;
					Component::Mask mask;
					uint32_t index;
					uint32_t count;
			};

			Scene(Engine & engine, Input & input, const std::string & sceneName);
			Entity entity(Entity::Id id);
			Entity::Id idOf(Entity::Index index);
			std::vector<Entity> entities(const std::string & name);
			Entity entity(const std::string & name);
			void destroy(Entity::Id id);
			size_t entityCount();

			template<typename T, typename... Vs>
			void attach(Entity::Index index, Vs&&... vs);

			template<typename T>
			T & component(Entity::Index idx);

			bool attached(Entity::Index index, const Component::Mask & testMask);

			void addProcessor(const std::string & procesorsName);
			void removeProcessor(const std::string & processorName);

			void registerComponent(Component::Id id, size_t size);
			void registerComponents(const std::vector<std::pair<Component::Id, size_t>> & idsAndSizes);

			void update();

			/**
			 * @brief   Returns an EntitySelect conforming to component types.
			 * @details It calculates the mask of all the component types passed
			 *	in. Its methods begin() and end() returns forward iterators to
			 *	be used in ranged-based loops.
			 *
			 * @example
			 * @code
			 * for (Entity e : scene.select<VelocityComponent, PositionComponent>())
			 * {
			 *	//...
			 * }
			 * @endcode
			 *
			 */
			template <typename... ComponentTypes> EntitySelector select();

		private:
			EntitySelector select(Component::Mask mask);

		private:
			Engine & engine;
			Input & input;

			// Manages entity id<->index stuff
			EntityIndex entityIndex;

			// Manages component allocations, by index
			ComponentPool componentPool;

			// Attached masks, by Entity index (not by id!)
			std::vector<Component::Mask> attachedMasks;

			// Processor instance attached to this scene
			std::vector<Fusion::Processor *> processors;

			struct
			{
				std::vector<Fusion::Processor *> keyboard;
				std::vector<Fusion::Processor *> mouse;
				std::vector<Fusion::Processor *> wheel;
			} processorByInput;

			// Name of attached processors
			std::unordered_map<std::string, Fusion::Processor *> processorByName;

			// Name of this scene
			std::string sceneName;
	};
}

#endif // FUSION_SCENE_H
