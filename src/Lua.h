#ifndef STICKMANS_LUA_H
#define STICKMANS_LUA_H

#include <string>
#include "LuaAccessor.h"

struct lua_State;

namespace Fusion
{
	class Lua
	{
		public:
			enum LuaTypes {
				Table,
				Number
			};
		public:
			Lua();
			~Lua();
			bool execute(const std::string & file);
			LuaAccessor operator[](const std::string & key);

			template<typename T> bool is(int index = -1) { return false; };
			template<int N> bool is(int index = -1) { return false; };
			template<typename T> T as(int index = -1) { return T(); };

			void field(int index, const std::string & key);
			template<typename T> void setField(int index, const std::string & key, const T & value);

			void global(const std::string & key);
			int top();

			void push(int value);
			void push(double value);
			void push(unsigned value);
			void push(const std::string & value) { push(value.c_str()); };
			void push(const char * value);
			void push(bool value);

			void pop(long n = 1);


		private:
			lua_State * L;
			void assign(int index, const std::string & key);
	};

	template<> bool Lua::is<int>(int index);
	template<> bool Lua::is<Lua::Table>(int index);
	template<> int Lua::as<int>(int index);

	template<> inline bool Lua::is<Lua::Number>(int index) { return is<int>(index); }

	template<typename T> void Lua::setField(int index, const std::string & key, const T & value)
	{
		push(value);
		assign(index, key);
	}
}

#endif //STICKMANS_LUA_H
