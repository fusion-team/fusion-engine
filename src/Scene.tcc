#ifndef FUSION_SCENE_TCC
#define FUSION_SCENE_TCC

#include "Scene.h"
#include "Component.h"
#include <cassert>

namespace Fusion {

	template<typename T, typename... Vs>
	void Scene::attach(Entity::Index index, Vs&&... vs)
	{
		assert(T::id > 0 && "This component was not registered.");
		Component::Id cid = T::id;
		uint8_t * mem = componentPool[cid][index];
		new(mem) T(std::forward<Vs>(vs)...);
		while (index >= attachedMasks.size())
			attachedMasks.resize(attachedMasks.size() * 2);
		attachedMasks[index][T::id -1] = 1;
	}

	template<typename T>
	T & Scene::component(Entity::Index idx)
	{
		ComponentPool::ComponentStorage & storage = componentPool[T::id];
		uint8_t * mem = storage[idx];
		return *(static_cast<T *>(static_cast<void *>(mem)));
	}

	template<typename... ComponentTypes>
	Scene::EntitySelector Scene::select()
	{
		return select(Component::maskOf<ComponentTypes...>());
	}
}

#endif //FUSION_SCENE_TCC
