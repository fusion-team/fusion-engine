#ifndef FUSIONMATH_H
#define FUSIONMATH_H

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

namespace Fusion
{
	namespace Math = glm;
}

#endif
