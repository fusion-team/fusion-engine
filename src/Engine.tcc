#include "Engine.h"
#include "Scene.h"
#include <type_traits>
#include "logtrace.h"

namespace Fusion
{
	namespace detail
	{
		template <typename T>
		void calculateComponentSizes(std::vector<std::pair<Component::Id, size_t>> & sizes)
		{
			sizes.emplace_back(T::id, sizeof(T));
		}

		template <typename T, typename Q, typename... Ts>
		void calculateComponentSizes(std::vector<std::pair<Component::Id, size_t>> & sizes)
		{
			sizes.emplace_back(T::id, sizeof(T));
			calculateComponentSizes<Q, Ts...>(sizes);
		}

		template <typename T>
		void checkBases()
		{
			static_assert(std::is_base_of<Component, T>::value, "A component class must have the Component class as base");
		}

		template <typename T, typename Q, typename... Ts>
		void checkBases()
		{
			static_assert(std::is_base_of<Component, T>::value, "A component class must have the Component class as base");
			checkBases<Q, Ts...>();
		}



		template <typename T>
		Component::Id assignComponentIds(Component::Id first)
		{
			T::id = first;
			first++;
			return first;
		}

		template <typename T, typename Q, typename... Ts>
		Component::Id assignComponentIds(Component::Id first)
		{
			T::id = first;
			first++;
			return assignComponentIds<Q, Ts...>(first);
		}
	}

	template <typename T>
	void Engine::registerComponent() {
		detail::checkBases<T>();
		nextId++;
		T::id = nextId;
		size_t size = sizeof(T);
		componentSizes.push_back(size);
		for (auto && i : scenes)
		{
			i.second->registerComponent(T::id, size);
		}
	}

	template <typename T, typename Q, typename... Ts>
	void Engine::registerComponents() {
		detail::checkBases<T, Q, Ts...>();
		nextId++;
		std::vector<std::pair<Component::Id, size_t>> sizes;
		detail::assignComponentIds<T, Q, Ts...>(nextId);
		detail::calculateComponentSizes<T, Q, Ts...>(sizes);

		for (auto && s : sizes)
		{
			assert(s.first == nextId);
			componentSizes.push_back(s.second);
			nextId++;
		}

		for (auto && i : scenes)
		{
			i.second->registerComponents(sizes);
		}
	}

	template <typename T>
	void Engine::registerProcessor(const std::string & processorName)
	{
		static_assert(std::is_base_of<Processor, T>::value, "A Processor must have the Processor class as base!");
		Processor *& p = registeredProcessors[processorName];
		if (p != nullptr)
		{
			modwarn("engine");
			trace("Processor", processorName, "being re-registered!");
			delete p;
		}
		p = new T();
		p->metadata.keyboardListener = !std::is_same<decltype(&Processor::onKeyboardInput), decltype(&T::onKeyboardInput)>::value;
		p->metadata.pointerListener = !std::is_same<decltype(&Processor::onPointerInput), decltype(&T::onPointerInput)>::value;
		p->metadata.wheelListener = !std::is_same<decltype(&Processor::onWheelInput), decltype(&T::onWheelInput)>::value;
	}
}
