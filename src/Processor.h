#ifndef FUSION_PROCESSOR_H
#define FUSION_PROCESSOR_H

#include "InputTriggers.h"
#include "FusionMath.h"
#include "Input.h"

namespace Fusion
{
	class Scene;
	/**
	 * @brief The Processor class is responsible for processing entities.
	 */
	class Processor
	{
		public:
			struct Metadata
			{
				public:
					bool keyboardListener;
					bool pointerListener;
					bool wheelListener;
					bool threadSafe;
			};

		public:
			Processor();
			virtual ~Processor();
			virtual void run(Scene & scene) = 0;

			virtual void onKeyboardInput(Input::PressState, KeyboardKey) { }
			virtual void onPointerInput(const Input::Pointer & pointer) { }
			virtual void onWheelInput(Math::ivec2) { }

			Metadata metadata;
	};

}
#endif // PROCESSOR_H
