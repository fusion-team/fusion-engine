LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := fusion

define walk
  $(wildcard $(1)) $(foreach e, $(wildcard $(1)/*), $(call walk, $(e)))
endef

ALL_FILES := $(call walk, $(LOCAL_PATH))
SRC_LIST := $(filter %.cpp, $(ALL_FILES)) $(filter %.cc, $(ALL_FILES)) $(filter %.c, $(ALL_FILES))
                          
LOCAL_C_INCLUDES := 

LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%, %,$(SRC_LIST)) 


LOCAL_EXPORT_C_INCLUDES := $(sort $(dir $(filter %.hpp, $(ALL_FILES)) $(filter %.h, $(ALL_FILES) ) )) \
                           jni/glm jni/fusion-engine/contrib/glad/include/ jni/lua/src
                           
LOCAL_C_INCLUDES := ${LOCAL_EXPORT_C_INCLUDES}

LOCAL_MODULE_FILENAME := libfusion

LOCAL_CFLAGS += -DLOGTRACE

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image
LOCAL_STATIC_LIBRARIES := SDL2_static SDL2_image

LOCAL_EXPORT_LDLIBS := -lGLESv1_CM -lGLESv3 -llog

include $(BUILD_STATIC_LIBRARY)
