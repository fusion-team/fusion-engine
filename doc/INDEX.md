# The Fusion Engine documentation files
 
 * INDEX.md (this file): Lists all the available documentation
 * COMPONENTS.md: Describes how to create, register and work with components
 * ENTITY.md: A guide on how to create and manage entities
 * INPUT.md: How to work with the `Input` class
 * GOTCHAS.md: Important notes about some peculiarities that might get you.