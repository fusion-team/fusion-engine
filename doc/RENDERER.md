# Fusion Rendering Classes

 The `Renderer` class is designed to be the pillar of Fusion's specialized renderers, thus it only offers the most basic functionalities. It lets you compile and link GLSL shaders into a ShaderProgram, upload vertexes (containing position, color and texture mappings) and to upload textures.

 Currently Fusion only supports 32 bits per pixel RGBA textures. You'll get strange results and artifacts if you don't upload textures in this format.

 The instance of the `Renderer` class can be obtaining by calling its static method `instance()`:

 ```cpp
Renderer & renderer = Renderer::instance();
```

## Shaders

 Fusion lets you use your shaders to render. To do that, you first need to acquire a `ShaderProgram` instance, by asking the `Renderer` instance to do it:

```cpp
const char * fShaderCode; // Load this code from file or hard-code it.
const char * vShaderCode; // Load this code from file or hard-code it.
Renderer::ShaderProgram p = renderer.compileShaders(vShaderCode, fShaderCode);

renderer.render(transform, mesh, p);
```

The `Renderer` class does not do any form of caching. If you ask to compile a shader twice, it will be compiled twice.

You can assign values to uniform variables in a shader program by using the `ShaderProgram::uniform` member, accessing them via the index operator:

```cpp
program.uniform["position"] = Fusion::Math::vec3{0.0f, 1.0f, 0.1f};
```

Note that uniform assignment will fail if types mismatch.

### Vertex shader variables

There are three vertex shader variables that are used to comunicate vertex data between the engine and the shader program:

- `Vertex`: must be declared as `in vec3 Vertex` in your vertex shader. It will be assigned with the (non-transformed) position of the vertex.
- `VertexNormal`: must be declared as `in vec3 VertexNormal`. It will be assigned with the normal vector of the vertex being processed.
- `VertexColor`: must be declared as `in vec4 VertexColor`. It will be assigned with the color of the vertex being processed. You can use this variable to pass interpolated colors to the fragment shader.
- `VertexTextureMap`: must be declared as `in vec2 VertexTextureMap`. It will be assigned with the st/uv mapping to be used in texture sampler. You should use this variable to pass interpolated texture mappings to your fragment shader.
- `ModelMatrix`: must be declared as `in mat4 ModelMatrix`. It will be assigned with the affine transform matrix of the model.
- `ViewMatrix`: must be declared as `in mat4 ViewMatrix`. It will be assigned with the matrix to convert from world space to camera space.
- `ProjectionMatrix`: must be delcared as `in mat4 ProjectionMatrix`. It will be assigned with the matrix the projection mode.

If you don't use any of these variables, it is safe to omit them.

Your vertex shader *must* output a variable called `gl_Position`, which is a `vec4`.

## Meshes

 Fusion's `Renderer` class lets you upload a vector of vertices containing position, color and texture mapping information. To specify a vertex, the `Vertex` class is used. You also need to upload a index vector that will be used to generate each triangle.

```cpp
// Specify a rectangle by means of two triangles.
std::vector<Renderer::Vertex> vertices = {
	{ // vertex 0
		{ -0.5f, 0.5f, 0.0f }, // position of vertex 0
	},
	{ // vertex 1
		{ 0.5f, 0.5f, 0.0f }, // position of vertex 1
	},
	{ // vertex 2
		{ 0.5f, -0.5f, 0.0f }, // position of vertex 2
	},
	{ // vertex 3
		{ -0.5f, -0.5f, 0.0f }, // position of vertex 3
	}
};

// Specify the index vector to define faces of the mesh
std::vector<unsigned> idx =  { 0, 2, 3, 0, 1, 2 };

Renderer::Mesh mesh = renderer.uploadMesh(vertices, idx);
```

## Textures
 
 The `Texture` class holds a reference to a GPU texture. You can obtain a `Texture` instance by calling `Renderer::uploadTexture`:

```cpp
// Load pixel data either by the ImageLoader class or yourself
Texture t = renderer.uploadTexture(pixels, width, height)
bool valid = t.id() > 0;
```

Before using a texture in a shader program, you first have to activate it. You can do that by using the method `Texture::activate`. Activating a texture will return a texture unit ID. You should then set this ID in the uniform. `Texture::activate()` returns non-zero for successful activations, and zero if no more texture units are available.

```cpp
Renderer::Texture t; // previously uploaded texture
uint32_t unit = t.activate();
if (unit > 0)
  shader.uniform["texture0"] = unit;
else
  // could not activate texture!
```

A shortcut to that is assigning a texture directly to a uniform:

```cpp
Renderer::Texture texture; // previously uploaded texture
shader.uniform["texture0"] = texture;
```

The downside is that it is impossible to check if the assignment was successful. If logging is enable, the engine will log that, though.

The texture will be activated and its texture unit will be passed to `"texture0"`.
Texture assignments are *temporary* and *are cleared* on after a `Renderer::swap()` call, so it is good practice to activate a texture before each render call.
Activating a texture multiple times has no cost, it will simply return the already used texture unit.

After using a texture, it is good practice to deactivate it, using the method `Texture::deactivate`:

```cpp
Renderer::Mesh mesh; // previously uploaded mesh
Renderer::Texture texture; // previously bound texture
shader.uniform["texture0"] = texture;
renderer.render(transform, mesh, shader);
texture.deactivate(); // free this texture slot.
```





	
