# Component guide

Components consists of blocks of data that, when attached to [Entities], they represent that Entity's game data. This game data is processed by [Processors], thus defining that Entity behaviour. This enables you to also use Components as if they were _tags_: by adding a Component to an Entity you're signaling for Processors that they should work on it.

## Creating a component

To create a component, one creates a class that inherits from the class `Component`. The second requirement is that this class **must have** an static field with the name `id` and type `Component::Id` that will be set when this component is registered.

Example:

```
// PositionComponent.h
class PositionComponent: Component
{
	static Component::Id id;
	float x, y, z;
};

// PositionComponent.cpp
#include "PositionComponent.h"
Component::Id PositionComponent::id;
```

## Component masks

Components are identified by an ID number. This number is unique and can be masked together with other Component's IDs to form a type of selector for functions that perform queries based on component types, for instance, `Scene::select` (See the [Scenes] document for more information).

To build a mask for a number of component types, you can use the function `maskOf`, available in the `Component` classs. Here's an example on how to use it:

```
// builds a component mask of VelocityComponent and AccelerationComponent
Component::Mask mask = Component::masOf<VelocityComponent | AccelerationComponent>();

// iterates over all entities containing those components
for (auto & entity : scene.select(mask))
{
	// do stuff with entity
}

```
