# Input system

Fusion input system is represented by the singleton class `Input`. By using it, you can query if a device has changed state in a frame, which keys are in which state and also to register `Processor` instances to be called whenever there are new input events.

## Trigger states

There are four states for keyboard keys and mouse buttons:

- Pressed: the trigger was pressed in this exact frame and it was not pressed before.
- Pressing: the trigger was pressed before and it is pressed now.
- Released: the trigger was pressed before but it is now released.
- Unpressed: the trigger was not pressed, nor pressing, nor released.

## Input query

The `Input` class supports keyboard and mouse state query:

```
Input & input = Input::instance();

if (input.pressed(KeyboardKey::Space))
{
    // handle KeyboardKey pressed
}

if (input.pressed(MouseButton::Left))
{
    // handle MouseButton pressed
}
```

## Reactive input handling

Classes that implement a `Processor` can override virtual methods `onPressed`, `onPressing`, `onReleased` and `onUnpressed` to handle these input events. You can safely ignore them if you are not interested in reactively handling events. Handling events in this way can become a performance bottleneck if you have a large number of active processors.

```
class MyProcessor : public Processor
{
    public:
        virtual void onPressed(Input::KeyboardKey k) { // a key was pressed }
        virtual void onPressed(Input::MouseButton b, int screenX, int screenY) { // a mouse button was pressed at screenX/screenY }
}
```

Processors are automatically registered to be called by the input class if you override these methods.
