# How to _<do important thing>_

This file has answers to some questions that you may never have. But if you have, its nice that you can find their answers here, right? Good luck.

## How to increase the maximum number of component types

To optimise memory and cache, Fusion cannot have an infinite number of component types (it might have an infinite number of instances of those components, though). The default value is 64, but it may not enough for you. To increase this value, you add the flag `-DMAXIMUM_NUMBER_OF_COMPONENT_TYPES=<number that you want>` to your compiler switches. Recompile both the engine AND your game with these switches.

