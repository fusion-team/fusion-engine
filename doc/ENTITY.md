# Entities

An entity represents an in-game object, be it a rock, an NPC or the main player. It is the conceptual glue that bind together components. When an entity has a component attached to it, its behavior is changed as it becomes sensible to Processors.

## The Entity class

An entity is, internally, represented by an _ID_ value and an _index_ value. An _ID_ never changes as long as its entity is not destroyed, _index_es on the other hand changes to preserve cache coherence and memory utilization. 

The `Entity` class carries only its index, meaning its very volatile. If you ever want to keep a reference to an entity, store its _ID_.
